var md5 = require('md5');


module.exports = app => {
    /**
     *todo list
     * 只允许一点登录
     */

    app.passport.verify(async (ctx, user) => {
        // 查找数据库，并验证密码是否正确
        /*
        const User = ctx.model.User;
        let foundUser = await User.findOne({ username: user.username });
        if(!foundUser || !foundUser.encryptPassword(user.password)) return false;
        return foundUser;
        */
        let result = await ctx.service.power.login(user);
        if (result) {
            return {id: result.id, name: result.name, roles: result.roles};
        } else {
            ctx.logout();
            return false
        }
    });
    //序列化与反序列化，序列化存储到session中只保存用户id
    app.passport.serializeUser(async (ctx, user) => {
        return user;
    });

    app.passport.deserializeUser(async (ctx, user) => {
        return user;
    });
};
