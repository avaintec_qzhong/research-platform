'use strict';


/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const {router, controller} = app;
    router.get('/', controller.view.html.index);
    // router.get('/logout', controller.login.power.admin);
    const localStrategy = app.passport.authenticate('local', {
        successRedirect: '/loginCallback',
        failureRedirect: '/loginCallback',
    });
    router.post('/login', localStrategy);
    router.get('/loginCallback', controller.api.power.login);
    router.get('/loginErrorCallback', controller.api.power.loginError);
    router.get('/logOut', controller.api.power.logOut);
    /**
     *about menus
     */
    router.post('/api/getAllMenus', controller.api.menus.getAllMenus);
    router.post('/api/getOpenMenusByRoule', controller.api.menus.getOpenMenusByRoule);
    router.post('/api/getOpenMenus', controller.api.menus.getOpenMenus);
    router.post('/api/updateMenus', controller.api.menus.updateMenus);

    /**
     * about roles
     */
    router.post('/api/addRole', controller.api.roles.addRole);
    router.get('/api/deleteRole', controller.api.roles.deleteRole);
    router.post('/api/editRole', controller.api.roles.editRole);
    router.get('/api/getRoles', controller.api.roles.getRoles);
    router.post('/api/editRoleMenus', controller.api.roles.editRoleMenus);

    /**
     * about mender
     */
    router.post('/api/addMember', controller.api.member.addMember);
    router.get('/api/deleteMember', controller.api.member.deleteMember);
    router.post('/api/editMember', controller.api.member.editMember);
    router.get('/api/getMembers', controller.api.member.getMembers);

    /**
     * about collectionProjects
     */
    router.post('/api/addCollectionProjects', controller.api.collectionProjects.addCollectionProjects);
    router.get('/api/deleteCollectionProjects', controller.api.collectionProjects.deleteCollectionProjects);
    router.post('/api/editCollectionProjects', controller.api.collectionProjects.editCollectionProjects);
    router.get('/api/getCollectionProjects', controller.api.collectionProjects.getCollectionProjects);

    /**
     * about collectionItems
     */
    router.post('/api/addcollectionItem', controller.api.collectionItem.addcollectionItem);
    router.get('/api/getcollectionItem', controller.api.collectionItem.getcollectionItem);
    router.get('/api/getcollectionItem_byId', controller.api.collectionItem.getcollectionItem_byId);
    router.get('/api/getCollectionForm_byId', controller.api.collectionItem.getCollectionForm_byId);
    router.post('/api/addcollectionItem_file', controller.api.collectionItem.addcollectionItem_file);
    /**
     * about dataReport
     */
    router.post('/api/addDataTotable', controller.api.dataStorage.addDataTotable);
    router.get('/api/getDataBase', controller.api.dataStorage.getDataBase);
    //router.get('/api/getFileData', controller.api.dataStorage.getFileData);

};


