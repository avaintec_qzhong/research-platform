'use strict';
const Service = require('egg').Service;
var md5 = require('md5');
const createId = require('../untils/createId');

class collectionItemService extends Service {
    async addcollectionItem(params) {
        /**
         *
         * 0，未审核
         * 1，进行中
         * 3，终止，
         * 4，完成
         * 5，异常
         *
         */
        let id = createId();
        let sql = `INSERT INTO collection_item (id,item_title,item_describe,fill_type,fill_members,collection_json,create_people_name,create_people_id,project_id,collcetion_type) 
        VALUES ('${id}',
        '${params.item_title}',
        '${params.item_describe}',
        '${params.fill_type}',
        '${params.fill_type === 'followUp' ? "" : params.fill_members.join(',')}',
        '${params.collection_json}',
        '${params.create_people_name}',
        '${params.create_people_id}',
        '${params.project_id}',
        'form'
        )`;
        let result = await this.app.mysql.get('platform').query(sql);
        result.id = id;
        return result
    }

    async addcollectionItem_file(params) {
        console.log(params);

        const {ctx} = this;
        const parmas_check = {
            item_title: "string",//数据名字
            file_path: "string",//文件存放路径
            project_id: "string",//项目id
            item_describe: "string",//文件描述
            create_people_name: "string",//
            create_people_id: "string",//
        };
        ctx.validate(parmas_check, params);
        let id = createId();
        let sql = `INSERT INTO collection_item (id,item_title,item_describe,create_people_name,create_people_id,project_id,file_path,collcetion_type) 
        VALUES ('${id}',
        '${params.item_title}',
        '${params.item_describe}',
        '${params.create_people_name}',
        '${params.create_people_id}',
        '${params.project_id}',
        '${params.file_path}',
        'dataFile'
        )`;
        let result = await this.app.mysql.get('platform').query(sql);
        result.id = id;
        return result
    }

    async editcollectionItem() {
    }

    async getcollectionItem(params) {
        let sql = `SELECT id,item_title,item_describe,fill_type,fill_members,create_people_name,create_people_id,project_id,collcetion_type
        from collection_item 
        WHERE project_id='${params.project_id}';`;

        /**
         * 应该补充数据的使用人员字段
         */
        /*let sql = `SELECT id,item_title,item_describe,fill_type,fill_members,create_people_name,create_people_id,project_id
        from collection_item
        WHERE project_id='${params.project_id}'
        AND (FIND_IN_SET('${params.id}',fill_members) OR create_people_id='${params.id}');`;*/
        return await this.app.mysql.get('platform').query(sql);
    }

    async getcollectionItem_byId(params) {
        let sql = `SELECT id,item_title,item_describe,fill_type,fill_members,create_people_name,create_people_id,project_id,collcetion_type
        from collection_item
        WHERE FIND_IN_SET('${params.id}',fill_members);`;
        return await this.app.mysql.get('platform').query(sql);
    }

    async getCollectionForm_byId(params) {
        let sql = `SELECT *
        from collection_item
        WHERE id='${params.id}';`;
        let result = await this.app.mysql.get('platform').query(sql);
        return result[0]
    }

}

module.exports = collectionItemService;






