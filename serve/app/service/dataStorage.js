'use strict';
const Service = require('egg').Service;
const createId = require('../untils/createId');
const fs = require('fs');
const readline = require('readline');

class dataStorageService extends Service {

    async createTable(collection_json, id) {
        /*let collection_json = [
            [{
            "controlInfo": ["姓名", "name"],
            "selfControlInfo": false,
            "label": null,
            "params": null,
            "necessary": false,
            "type": "Input",
            "value": "String",
            "keyValue": [],
            "placeholder": null,
            "ref": { "current": null }
        }],
            [{
                "controlInfo": ["姓名", "name"],
                "selfControlInfo": false,
                "label": null,
                "params": null,
                "necessary": true,
                "type": "Radio",
                "value": "String",
                "keyValue": [["男", "man"], ["女", "woman"]],
                "placeholder": null
            }],
            [{
                "controlInfo": ["既往史", "oldIllness"],
                "selfControlInfo": false,
                "label": null,
                "params": null,
                "necessary": false,
                "type": "Checkbox",
                "value": "String",
                "keyValue": [["既往史", "oldIllness"], ["现病史", "chiefComplaint"], ["药物史", "medicalHistory"]],
                "placeholder": null
            }]];*/
        let collection_array = JSON.parse(collection_json);
        collection_array = collection_array.reduce((pre, nowItem) => {
            return pre.concat(nowItem)
        }, []);
        let keysStr = [];
        collection_array.forEach((elementConfig) => {
            switch (elementConfig.type) {
                case null:
                    break;
                case "InputNumber":
                    keysStr.push(`${elementConfig.controlInfo[1]} int `);
                    break;
                default:
                    keysStr.push(`${elementConfig.controlInfo[1]} varchar(255) `);
            }
        });
        let sql = "";
        if (keysStr.length === 0) {
            sql = `CREATE TABLE data_${id}
                    (
                    id varchar(11)
                    )`;
        } else {
            sql = `CREATE TABLE data_${id}
                    (
                    id varchar(11), ${keysStr}
                    )`;
        }
        keysStr = keysStr.join(',');
        /*let sql = `CREATE TABLE Persons
                    (
                    id int,
                    Id_P int,
                    LastName varchar(255),
                    FirstName varchar(255),
                    Address varchar(255),
                    City varchar(255)
                    )`*/
        let result = await this.app.mysql.get('dataStorage').query(sql);
        return result
    }

    async addDataTotable(params, dataBase_id) {
        let columns = [];
        let values = [];
        params.forEach((elements) => {
            columns.push(elements.title);
            switch (elements.type) {
                case "InputNumber":
                    values.push(`${elements.value ? elements.value : 0}`);
                    break;
                default:
                    values.push(`'${elements.value ? elements.value : null}'`);

            }
        });
        columns = columns.join(',');
        values = values.join(',');
        console.log(columns);
        console.log(values);
        let data_id = createId();
        let sql = `INSERT INTO data_${dataBase_id}(id,${columns}) VALUES('${data_id}',${values});`;
        let result = await this.app.mysql.get('dataStorage').query(sql);
        return result;
    }

    async getDataBase(dataBase_id) {
        let sql = `SELECT * FROM data_${dataBase_id}`;
        let result = await this.app.mysql.get('dataStorage').query(sql);
        return result;
    }


    //方法问题----中间you
    async loadDataFromFile(params) {
        let table_name = `data_${params.id}`;
        let tow_lines = await this.readFileToArr(params.file_path);

        let colnums = tow_lines[0].split(`,`).map((key) => {
            if (key === `id`) {
                return '_id'
            } else {
                return key.replace(/^(")*|(")*$/g, "");
            }
        });
        let types = tow_lines[1].split(`,`).map((value) => (isNaN(value) ? `varchar(255)` : `int(255)`));
        if (colnums.length !== types.length) {
            return 'err'
        }
        let colnums_type = [];
        colnums.forEach((key, index) => {
            colnums_type.push(`${key} ${types[index]}`)
        });
        colnums_type = colnums_type.join(`,`);

        /*let createTable = `CREATE TABLE ${table_name}
                            (
                            id INT AUTO_INCREMENT PRIMARY KEY ,
                            ${colnums_type}
                            )`;*/

        let createTable = `CREATE TABLE ${table_name}
                            (
                            id INT AUTO_INCREMENT PRIMARY KEY ,
                            ${colnums_type}
                            )ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
        await this.app.mysql.get('dataStorage').query(createTable);

        let pathxx = params.file_path.replace(/\\/g, '\\\\');
        let sql = `load data infile '${pathxx}' 
                into table ${table_name} 
                fields terminated by ',' 
                optionally enclosed by '"' 
                ignore 1 lines
                (${colnums.join(',')});`;

        console.log(sql);
        let result = await this.app.mysql.get('dataStorage').query(sql);
        return result;
    }

    async _loadDataFromFile(params) {

    }

    async getKyes() {
        let sql = `select COLUMN_NAME from information_schema.COLUMNS where table_name="roles";`
    }

    async getFileColnums(path) {
    }

//从文件中提取两行文本，使用封装为async await
    readFileToArr(file_path) {
        return new Promise((resolve, reject) => {
            let fRead = fs.createReadStream(file_path);
            let objReadline = readline.createInterface({
                input: fRead
            });
            let arr = [];
            objReadline.on('line', (line) => {
                arr.push(line);
                if (arr.length === 2) {
                    objReadline.close();
                    objReadline.removeAllListeners();
                    fRead.destroy();
                }
            });
            objReadline.on('close', () => {
                resolve(arr);
            });
            objReadline.on('err', (err) => {
                reject(err);
            });
            fRead.on('close', () => {

            });

        });
    }


    fileToDatabase(params) {
        let table_name = `data_${params.id}`;
        let file_path = params.file_path;
        let value_lengh = 0;
        return new Promise((resolve, reject) => {
            let fRead = fs.createReadStream(file_path);
            let objReadline = readline.createInterface({
                input: fRead
            });
            let arr = [];
            let insert_colums = null;
            let create_table_flag = true;
            objReadline.on('line', (line) => {
                arr.push(line);
                if (arr.length === 2 && create_table_flag) {//创建数据库
                    let colnums = arr[0].split(`,`).map((key) => {
                        if (key === `id`) {
                            return '_id'
                        } else {
                            return key.replace(/^(")*|(")*$/g, "");
                        }
                    });
                    insert_colums = colnums.join(`,`);
                    let types = arr[1].split(`,`).map((value) => (isNaN(value) ? `varchar(255)` : `int(255)`));
                    value_lengh = types.length;
                    if (colnums.length !== types.length) {
                        return 'err'
                    }
                    let colnums_type = [];
                    colnums.forEach((key, index) => {
                        colnums_type.push(`${key} ${types[index]}`)
                    });
                    colnums_type = colnums_type.join(`,`);
                    let createTable = `CREATE TABLE ${table_name}
                            (
                            id INT AUTO_INCREMENT PRIMARY KEY ,
                            ${colnums_type}
                            )ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
                    arr = [arr[1]];
                    create_table_flag = false;
                    this.app.mysql.get('dataStorage').query(createTable);
                } else if (arr.length === 100) {
                    let sql_values = [];
                    arr.forEach((str) => {
                        let newLines = str.split(`,`);
                        if (newLines.length === value_lengh) {
                            let line_sql_value=`(${newLines.map((value) => (isNaN(value) ? `'${value}'` : `${value}`)).join(',')})`;
                            sql_values.push(line_sql_value);
                        }
                    });
                    let sql = `INSERT INTO ${table_name}(${insert_colums}) VALUES
                    ${sql_values.join(`,`)};`;
                    this.app.mysql.get('dataStorage').query(sql);
                }
            });
            objReadline.on('close', () => {
                let sql_values = [];
                arr.forEach((str) => {
                    let newLines = str.split(`,`);
                    if (newLines.length === value_lengh) {
                        let line_sql_value=`(${newLines.map((value) => (isNaN(value) ? `'${value}'` : `${value}`)).join(',')})`;
                        sql_values.push(line_sql_value);
                    }
                });

                let sql = `INSERT INTO ${table_name}(${insert_colums}) VALUES
                    ${sql_values.join(`,`)};`;
                this.app.mysql.get('dataStorage').query(sql);
                resolve('ok')
            });
            objReadline.on('err', (err) => {
                reject(err);
            });
            fRead.on('close', () => {

            });

        });
    }
}

module.exports = dataStorageService;

