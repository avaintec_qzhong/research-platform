'use strict';
const Service = require('egg').Service;
var md5 = require('md5');
const createId = require('../untils/createId');

class CollectionProjectsService extends Service {
    async addCollectionProjects(params) {

        /**
         *
         * 0，未审核
         * 1，进行中
         * 3，终止，
         * 4，完成
         * 5，异常
         *
         */
        let id = createId();
        let sql = `INSERT INTO collection_projects (id,project_name,project_describe,applicant_name,applicant_id,members_id,project_status) 
        VALUES ('${id}',
        '${params.project_name}',
        '${params.project_describe}',
        '${params.applicant_name}',
        '${params.applicant_id}',
        '${params.applicant_id}',
        1)`;
        return await this.app.mysql.get('platform').query(sql);
    }

    async updateCollectionProjects() {
    }

    async editCollectionProjects() {
    }

    async getCollectionProjects(params) {
        let sql = `SELECT * from collection_projects WHERE FIND_IN_SET('${params.id}',members_id);`;
        return await this.app.mysql.get('platform').query(sql);
    }
}

module.exports = CollectionProjectsService;





