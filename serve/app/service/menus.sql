/*
 Navicat Premium Data Transfer

 Source Server         : research_platform
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : research_platform

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 10/04/2020 14:02:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collection_item
-- ----------------------------
DROP TABLE IF EXISTS `collection_item`;
CREATE TABLE `collection_item`  (
  `id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `item_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `item_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `fill_members` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `fill_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `collection_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `collection_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `fill_rule` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `create_people_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_people_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for collection_projects
-- ----------------------------
DROP TABLE IF EXISTS `collection_projects`;
CREATE TABLE `collection_projects`  (
  `id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `applicant_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `applicant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `apply_time` datetime(0) NULL DEFAULT NULL,
  `project_target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_files_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_files_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `members_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `datas_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `project_status` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collection_projects
-- ----------------------------
INSERT INTO `collection_projects` VALUES ('7ngp7h7j8', '测试', NULL, '测试效果', 'guanli', 'sx3vx6arj', NULL, NULL, NULL, NULL, 'sx3vx6arj', NULL, 1);

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `roles` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `resetFlag` tinyint(255) NULL DEFAULT NULL,
  `sdandy_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sdandy_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sdandy_3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('ia9y3a8jy', 'caiji', 'man', '11', '11', 11, '111', 'eqo51vefm', '14e1b600b1fd579f47433b88e8d85291', 1, NULL, NULL, NULL);
INSERT INTO `members` VALUES ('pdhucqlfq', 'keyan', 'man', 'xx', 'x', 1, '0000', 'pdhy7bd2u', '14e1b600b1fd579f47433b88e8d85291', 1, NULL, NULL, NULL);
INSERT INTO `members` VALUES ('sx3vx6arj', 'guanli', 'man', '1', '1', 1, 'sss', 'wgptmydqs', '14e1b600b1fd579f47433b88e8d85291', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `en_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `parents_id` int(11) NULL DEFAULT NULL,
  `chid_id` int(11) NULL DEFAULT NULL,
  `belong_roles` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `switch_status` tinyint(255) NULL DEFAULT NULL,
  `standy_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standy_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standy_3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, '驾驶舱xxx', 'overview', 'overview', 'heat-map', 0, NULL, NULL, 0, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (2, '全院科研概览', 'Overview of the hospital ', '1', 'heat-map', 1, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (3, '科室科研概览', 'Department overview', '2', 'box-plot', 1, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (4, '成果汇报', 'Results report', '3', 'android', 1, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (5, '数据采集系统', 'collection', 'collection', 'sliders', 0, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (6, '采集项目列表', 'collection', 'collectionProjects', 'windows', 5, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (7, '数据收集列表', 'CollectionItem', 'CollectionItem', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (8, '数据展示', 'data', 'data', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (9, '数据模板设计', 'DesignFormItem', 'DesignFormItem', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (10, '上传数据集', 'UploadFiles', 'UploadFiles', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (11, '数据库采集', 'DataBaseCollection', 'DataBaseCollection', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (12, '数据模板编辑', 'EditFormItem', 'EditFormItem', 'windows', 6, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (13, '数据填报列表', 'CollectionItem', 'CollectionItem', 'windows', 5, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (14, '数据上报界面', 'DesignFormItem', 'DesignFormItem', 'windows', 13, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (15, '数据上报', 'FillFormItem', 'FillFormItem', 'windows', 13, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (16, '大数据科研平台', 'research', 'research', 'chrome', 0, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (17, '科研项目', 'research', 'ResearchProject', 'windows', 16, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (18, '项目课题表列表', 'ResearchItem', 'ResearchItem', 'windows', 17, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (19, '数据分析列表', 'ResearchFlatChart', 'ResearchFlatChart', 'windows', 17, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (20, '汇报文档百编辑', 'ReportEditor', 'ReportEditor', 'windows', 17, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (21, '个人项目', 'PersonProjectList', 'PersonProjectList', 'windows', 16, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (22, '项目课题表列表', 'PersonResearchItem', 'PersonResearchItem', 'windows', 21, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (23, '数据分析列表', 'PersonResearchFlatChart', 'PersonResearchFlatChart', 'windows', 21, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (24, '汇报文档百编辑', 'ReportEditor', 'ReportEditor', 'windows', 21, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (25, '研究报告', 'Report', 'Report', 'windows', 16, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (26, '项目课题表列表', 'PersonResearchItem', 'PersonResearchItem', 'windows', 25, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (27, '科研管理系统', 'management', 'management', 'edit', 0, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (28, '公用字典', '公用字典', 'DictinonaryManage', 'windows', 27, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (29, '科研项目管理', '科研项目管理', 'ProjectMange', 'windows', 27, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (30, '项目信息', 'ProjectDetails', 'ProjectDetails', 'windows', 29, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (31, '数据项目管理', '数据项目管理', 'DataCollectionManage', 'windows', 27, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (32, '角色权限管理', '角色权限管理', 'RoleManage', 'windows', 27, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (33, '成员管理', '成员管理', 'MemberManage', 'windows', 27, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (34, '后台管理', 'platformManagement', 'platformManagement', 'laptop', 0, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (35, '导航管理', 'navigationManagement', 'navigationManagement', 'slack', 34, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `menus` VALUES (36, '超级角色管理', '角色权限管理', 'SuperRoleManagement', 'windows', 34, NULL, NULL, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `describle` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `members_numbers` int(11) NULL DEFAULT NULL,
  `switch_status` tinyint(255) NULL DEFAULT NULL,
  `creation_time` datetime(0) NULL DEFAULT NULL,
  `own_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standy_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standy_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standy_3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('eqo51vefm', '数据录入人员', '录入数据使用', NULL, NULL, NULL, '13,14,15,5', NULL, NULL, NULL);
INSERT INTO `roles` VALUES ('pdhy7bd2u', '科研人员', '科研', NULL, NULL, NULL, '5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26', NULL, NULL, NULL);
INSERT INTO `roles` VALUES ('wgptmydqs', '科研管理人员', '管理端', NULL, NULL, NULL, '27,28,29,30,31,32,33,16,5,6,13,7,8,9,10,11,12,14,15,17,18,19,20,21,22,23,24,25,26', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
