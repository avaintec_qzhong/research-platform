'use strict';
const Service = require('egg').Service;

class getMenusService extends Service {
    async getAllmenus(params) {
        let sql = null;
        if (params && params.role) {
            sql = `SELECT 
        id , name , en_name ,  path , icon , parents_id ,switch_status  
        FROM menus WHERE belong_roles like %${params.role}% ;`;
        } else {
            sql = `SELECT 
        id , name , en_name ,  path , icon , parents_id ,switch_status  
        FROM menus ;`;
        }
        const result = await this.app.mysql.get('platform').query(sql);

        return this.arrayToTree(result);

    }

    async getOpenMenus() {
        let sql = `SELECT
        id , name , en_name ,  path , icon , parents_id ,switch_status
        FROM menus
        WHERE switch_status=1;`;
        const result = await this.app.mysql.get('platform').query(sql);
        return this.arrayToTree(result);
    }

    async getOpenMenusByRoule(user) {
        let sql_role = `SELECT own_menu FROM roles WHERE id='${user.roles}'`;
        const own_menu = await this.app.mysql.get('platform').query(sql_role);
        let sql = `SELECT
        id,name,en_name,path,icon,parents_id,switch_status
        FROM menus
        WHERE switch_status=1 AND  id IN (${own_menu[0].own_menu});`;
        const result = await this.app.mysql.get('platform').query(sql);
        return this.arrayToTree(result);
    }

    async getMenusByrole() {
        console.log('hello');
    }

    async updateMenus(menuConfig) {
        let sql = `UPDATE menus 
             SET 
             name='${menuConfig.name}',
             icon='${menuConfig.icon}',
             switch_status=${menuConfig.switch_status}
             WHERE id=${menuConfig.id} ;`;
        const result = await this.app.mysql.get('platform').query(sql);
        return result

    }

    /**
     *  translate array to tree style json data
     *  There's a lot of room for improvement
     *  tolo list :
     *        Cache tree data
     *
     * @param {Egg.Application} app - egg application
     *
     */
    arrayToTree(data) {
        const menusTemp = [];
        for (let i = 0; i <= 40; i++) {
            const temp = [];
            data.forEach(obj => {
                if (obj.parents_id === i) {
                    temp.push(obj);
                }
            });
            if (temp.length > 0) {
                menusTemp.push(temp);
            }
        }

        /*  eslint-disable */

        while (menusTemp.length > 1) {
            let target = menusTemp.pop();
            _one:
                for (let index = 0; index < menusTemp.length; index++) {
                    _two:
                        for (let _index_ = 0; _index_ < menusTemp[index].length; _index_++) {
                            if (menusTemp[index][_index_].id === target[0].parents_id) {
                                menusTemp[index][_index_].child = target;
                                break _one;
                            }
                        }
                }
            ;
        }
        //console.log(menusTemp)
        return menusTemp[0];
        /* eslint-enable */
    }
}

module.exports = getMenusService;



