'use strict';
const Service = require('egg').Service;

// const createId=require('..')
class RolesService extends Service {
    async addRole(params) {
        let sql = `INSERT INTO roles (id,name,describle)
        VALUES ('${params.id}','${params.name}','${params.describle}')`;
        return await this.app.mysql.get('platform').query(sql);
    }

    async deleteRole(params) {
        let sql = `DELETE FROM roles 
        WHERE ID='${params.id}';`;
        return await this.app.mysql.get('platform').query(sql);
    }

    async editRole(params) {
        let sql = `UPDATE roles
        SET 
        name='${params.name}',
        describle='${params.describle}'
        WHERE ID='${params.id}';`;
        return await this.app.mysql.get('platform').query(sql);
    }

    async getRoles() {
        let sql = "SELECT * FROM roles WHERE name!='admin';";
        return await this.app.mysql.get('platform').query(sql);
    }

    async editRoleMenus(params) {
        //split
        let sql = `UPDATE roles
        SET
        own_menu='${params.own_menu.toString()}'
        WHERE ID='${params.id}';`;

        return await this.app.mysql.get('platform').query(sql);
    }
}

module.exports = RolesService;
