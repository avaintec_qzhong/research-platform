'use strict';
const Controller = require('egg').Controller;

/**
 *  updateMenusj api params  rule
 */
//rule type must be one of number, int, integer, string, id, date, dateTime, datetime, boolean, bool, array, object, enum, email, password, url, but the following type was passed: undefined


class menusController extends Controller {
    async getAllMenus() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            ctx.body = ctx.request.body;
            const newsList = await ctx.service.menus.getAllmenus();
            ctx.body = {
                txt: 'ok',
                data: newsList,
            };
        } else {
            ctx.body = {
                txt: 'unLogin',
            };
        }
    }

    async updateMenus() {
        const updateMenusRules = {
            name: 'string',
            icon: 'string',
            switch_status: 'boolean',
            id: 'int',
        };
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let requestConfig = ctx.request.body;
            ctx.validate(updateMenusRules, requestConfig);
            updateMenusRules.switch_status === true ? updateMenusRules.switch_status = 1 : updateMenusRules.switch_status = 0;
            const result = await ctx.service.menus.updateMenus(requestConfig);
            ctx.body = {
                status: 2,
                txt: '编辑成功',
                data: result,
            };
        } else {
            ctx.body = {
                status: 4,
                txt: '未登录',
            };
        }
    }

    async getOpenMenus() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            // ctx.body = ctx.request.body;
            const newsList = await ctx.service.menus.getOpenMenus(ctx.user);
            ctx.body = {
                txt: 'ok',
                data: newsList,
            };
        } else {
            ctx.body = {
                txt: 'unLogin',
            };
        }
    }

    async getOpenMenusByRoule() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            // ctx.body = ctx.request.body;
            const newsList = await ctx.service.menus.getOpenMenusByRoule(ctx.user);
            ctx.body = {
                txt: 'ok',
                data: newsList,
                user: ctx.user
            };
        } else {
            ctx.body = {
                txt: 'unLogin',
            };
        }
    }

}

module.exports = menusController;
