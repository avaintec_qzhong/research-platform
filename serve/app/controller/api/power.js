'use strict';
const Controller = require('egg').Controller;

class powerController extends Controller {

    async login() {
        const {ctx} = this;
        let user = ctx.user;
        if (ctx.isAuthenticated()) {
            console.log('---------login-------------');
            ctx.body = {
                code: 1,
                txt: '登录成功',
                data: {id: user.id, name: user.name, roles: user.roles}
            }
            ;
        } else {
            ctx.body = {
                code: 0,
                txt: '登录失败',
            };
        }
    }

    async logOut() {
        const ctx = this.ctx;
        ctx.logout();
        return ctx.body = {
            txt: 'logout',
            data: null
        };
    }


    async loginError() {
        const {ctx} = this;
        console.log('---------loginError-------------')
        console.log('ctx.user', ctx.user);
        ctx.body = {
            txt: '登录失败',
        };
    }


}


module.exports = powerController;
