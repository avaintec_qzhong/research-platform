'use strict';
const Controller = require('egg').Controller;


class dataStorageController extends Controller {

    async addDataTotable() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.body;
            let addParams = {
                dataBase_id: "string"
            };
            ctx.validate(addParams, postParams);
            console.log(postParams);
            const result = await ctx.service.dataStorage.addDataTotable(postParams.data, postParams.dataBase_id);

            return ctx.body = {
                txt: 'ok',
                data: result,
            };

        }
    }

    async getDataBase() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.query;
            let addParams = {
                dataBase_id: "string"
            };
            ctx.validate(addParams, postParams);
            const result = await ctx.service.dataStorage.getDataBase(postParams.dataBase_id);
            //await ctx.service.dataStorage.loadDataFormFile();

            return ctx.body = {
                txt: 'ok',
                data: result,
            };
        }
    }

    async getFileData() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.query;
            let addParams = {
                data_file_id: "string"
            };
            ctx.validate(addParams, postParams);

            const result = await ctx.service.dataStorage.getFileData(postParams.data_file_id);
            return ctx.body = {
                txt: 'ok',
                data: result,
            };
        }
    }

}

module.exports = dataStorageController;
