'use strict';
const Controller = require('egg').Controller;


class CollectionProjectsController extends Controller {
    async addCollectionProjects() {
        const addParams = {
            applicant_id: 'string',
            applicant_name: 'string',
            project_name: 'string',
            project_describe: 'string',
        };

        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.body;
            ctx.validate(addParams, postParams);
            const result = await ctx.service.collectionProjects.addCollectionProjects(postParams);
            return ctx.body = {
                txt: 'ok',
                data: result,
            };
        }
    }

    async deleteCollectionProjects() {
    }

    async editCollectionProjects() {
    }

    async getCollectionProjects() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const queryObj = this.ctx.query;
            const projectsList = await ctx.service.collectionProjects.getCollectionProjects(queryObj);
            return ctx.body = {
                txt: 'ok',
                data: projectsList,
            };
        }
    }

}

module.exports = CollectionProjectsController;
