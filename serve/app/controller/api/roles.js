'use strict';


const Controller = require('egg').Controller;
const createId = require('../../untils/createId');


class RolesController extends Controller {
    async addRole() {
        const addParmas = {
            name: 'string',
            describle: 'string',
        };
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.body;
            ctx.validate(addParmas, postParams);
            postParams.id = createId();
            const rolesList = await ctx.service.roles.addRole(postParams);
            return ctx.body = {
                status:2,
                txt: 'ok',
                data: rolesList,
            };
        }
    }

    async deleteRole() {
        const deleteParmas = {
            id: 'string',
        };
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const params = this.ctx.query;
            ctx.validate(deleteParmas, params);
            const rolesList = await ctx.service.roles.deleteRole(params);
            return ctx.body = {
                status:2,
                txt: '删除角色成功',
                data: rolesList,
            };
        }
    }

    async editRole() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const editParmas = {
                name: 'string',
                describle: 'string',
                id: 'string'
            };
            let postParams = ctx.request.body;
            ctx.validate(editParmas, postParams);
            const rolesList = await ctx.service.roles.editRole(postParams);
            return ctx.body = {
                status:2,
                txt: '编辑角色成功',
                data: rolesList,
            };

        }
    }

    async getRoles() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            //const newsList = await ctx.service.menusCRUD.getOpenMenus();
            const rolesList = await ctx.service.roles.getRoles();
            return ctx.body = {
                txt: 'ok',
                data: rolesList,
            };
        }
    }

    async editRoleMenus() {
        const {ctx} = this;
        const editParmas = {
            id: 'string',
            own_menu: 'array'
        };
        let postParams = ctx.request.body;
        ctx.validate(editParmas, postParams);
        const result = await ctx.service.roles.editRoleMenus(postParams);
        return ctx.body = {
            status:2,
            txt: '编辑角色菜单成功',
            data: result,
        };
    }


}

module.exports = RolesController;
