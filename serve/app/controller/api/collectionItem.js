'use strict';
const Controller = require('egg').Controller;
const fs = require('fs');
const path = require('path');
//const querystring =require('querystring');
const sendToWormhole = require('stream-wormhole');

class collectionItemController extends Controller {
    async addcollectionItem() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.body;
            let addParams = {};
            if (postParams.fill_type === 'followUp') {
                addParams = {
                    item_title: "string",
                    item_describe: "string",
                    fill_type: "string",
                    collection_json: "string",
                    create_people_name: "string",
                    create_people_id: "string",
                    project_id: "string",
                };
            } else {
                addParams = {
                    item_title: "string",
                    item_describe: "string",
                    fill_type: "string",
                    fill_members: "array",
                    collection_json: "string",
                    create_people_name: "string",
                    create_people_id: "string",
                    project_id: "string",
                };
            }
            ctx.validate(addParams, postParams);
            const result = await ctx.service.collectionItem.addcollectionItem(postParams);
            await ctx.service.dataStorage.createTable(postParams.collection_json, result.id);
            return ctx.body = {
                txt: 'ok',
                data: result,
            };

        }
    }

    async deletecollectionItem() {
    }

    async editcollectionItem() {
    }

    async getcollectionItem() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const queryObj = this.ctx.query;
            const projectsList = await ctx.service.collectionItem.getcollectionItem(queryObj);
            return ctx.body = {
                txt: 'ok',
                data: projectsList,
            };
        }
    }

    async getcollectionItem_byId() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const queryObj = this.ctx.query;
            let addParams = {
                id: "string",
            };
            ctx.validate(addParams, queryObj);
            const projectsList = await ctx.service.collectionItem.getcollectionItem_byId(queryObj);
            return ctx.body = {
                txt: 'ok',
                data: projectsList,
            };
        }
    }

    async getCollectionForm_byId() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const queryObj = this.ctx.query;
            let addParams = {
                id: "string",
            };
            ctx.validate(addParams, queryObj);
            const projectsList = await ctx.service.collectionItem.getCollectionForm_byId(queryObj);
            return ctx.body = {
                txt: 'ok',
                data: projectsList,
            };
        }
    }

    async addcollectionItem_file() {
        const {ctx} = this;
        let stream = await ctx.getFileStream();
        let file_save_name = new Date().getTime() + stream.filename; // stream对象也包含了文件名，大小等基本信息
        // 创建文件写入路径
        let file_path = path.join(__dirname, '../../../uploadDatas', file_save_name);
        const result = await new Promise((resolve, reject) => {
            // 创建文件写入流
            const remoteFileStrem = fs.createWriteStream(file_path);
            // 以管道方式写入流
            stream.pipe(remoteFileStrem);
            let errFlag;
            // 监听error事件
            remoteFileStrem.on('error', err => {
                errFlag = true;
                // 停止写入
                sendToWormhole(stream);
                remoteFileStrem.destroy();
                console.log(err);
                reject(err)
            });
            // 监听写入完成事件
            remoteFileStrem.on('finish', () => {
                if (errFlag) return;
                resolve({file_save_name, name: stream.fields.name})
            });
        });

        let params = {
            file_path,//文件存放路径
            item_title: stream.fields.item_title,//文件名字
            project_id: stream.fields.project_id,//项目名字
            create_people_id: stream.fields.create_people_id,//
            create_people_name: stream.fields.create_people_name,//
            item_describe: "====",//文件描述
        };

        /*生成item的表*/

        let item_result = await ctx.service.collectionItem.addcollectionItem_file(params);

        let file_data_params = {
            file_path,//文件存放路径
            id: item_result.id
        };

        // let file_to_table = await ctx.service.dataStorage.loadDataFromFile(file_data_params);
        let file_to_table = await ctx.service.dataStorage.fileToDatabase(file_data_params);
        /*文件灌入数据*/
        ctx.body = {code: 200, message: '', data: file_to_table}
    }

}

module.exports = collectionItemController;
