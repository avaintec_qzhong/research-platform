'use strict';

const Controller = require('egg').Controller;
const createId = require('../../untils/createId');

class menberController extends Controller {
    async addMember() {
        const addParmas = {
            name: 'string',
            sex: 'string',
            position: 'string',
            department: 'string',
            age: 'number',
            address: 'string',
            roles: 'array',
        };
        //id,name,sex,position,department,age,address
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            let postParams = ctx.request.body;
            ctx.validate(addParmas, postParams);
            postParams.id = createId();
            const rolesList = await ctx.service.member.addMember(postParams);
            return ctx.body = {
                status: 2,
                txt: 'ok',
                data: rolesList,
            };
        }
    }

    async deleteMember() {
        const deleteParmas = {
            id: 'string',
        };
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const params = this.ctx.query;
            ctx.validate(deleteParmas, params);
            const result = await ctx.service.member.deleteMember(params);
            return ctx.body = {
                status: 2,
                txt: 'ok',
                data: result,
            };
        }
    }

    async editMember() {
        const {ctx} = this;
        if (ctx.isAuthenticated()) {
            const editParmas = {
                name: 'string',
                sex: 'string',
                position: 'string',
                department: 'string',
                age: 'number',
                address: 'string',
                roles: 'array',
            };
            let postParams = ctx.request.body;
            ctx.validate(editParmas, postParams);
            const result = await ctx.service.member.editMember(postParams);
            return ctx.body = {
                status: 3,
                txt: 'ok',
                data: result,
            };

        }
    }

    async getMembers() {
        const {ctx} = this;
        if (ctx.isAuthenticated()){
            //const newsList = await ctx.service.menusCRUD.getOpenMenus();
            const rolesList = await ctx.service.member.getMembers();
            return ctx.body = {
                txt: 'ok',
                data: rolesList,
            };
        }
    }
}

module.exports = menberController;
