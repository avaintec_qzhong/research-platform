'use strict';

/** @type Egg.EggPlugin */
module.exports = {
    // had enabled by egg
    /**
     * 静态服务文件设定
     */
    static: {
        enable: true,
    },
    /**
     * 跨域
     */
    cors: {
        enable: true,
        package: 'egg-cors',
    },
    /**
     * mysql
     */
    mysql: {
        enable: true,
        package: 'egg-mysql',
    },
    /**
     * mysql
     */
    passport: {
        enable: true,
        package: 'egg-passport',
    },
    passportLocal: {
        enable: true,
        package: 'egg-passport-local',
    },
    /**
     * egg-validate
     * 参数校验插件
     */
    validate: {
        enable: true,
        package: 'egg-validate',
    }
}
;
