/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
    /**
     * built-in config
     * @type {Egg.EggAppConfig}
     **/
    const config = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_1584492765663_1829';

    config.assets = {
        publicPath: '/public/',
    };
    /**
     * 允许跨域访问,关闭csrf认证
     * @type {{csrf: {enable: boolean}}}
     */
    config.security = {
        csrf: {
            enable: false,
        },
    };
    config.cors = {
        // origin: '*',
        origin: 'http://127.0.0.1:4000',
        allowMethods: 'GET,HEAD,PUT,OPTIONS,POST,DELETE,PATCH',
        credentials: true,
    };
    /**
     * mysql配置
     */

    config.mysql = {
        // 数据库信息配置
        clients: {
            // clientId, 获取client实例，需要通过 app.mysql.get('clientId') 获取
            platform: {
                // host
                host: '127.0.0.1',
                // 端口号
                port: '3306',
                // 用户名
                user: 'root',
                // 密码
                password: '123456',
                // 数据库名
                database: 'research_platform',
            },
            dataStorage: {
                // host
                host: '127.0.0.1',
                // 端口号
                port: '3306',
                // 用户名
                user: 'root',
                // 密码
                password: '123456',
                // 数据库名
                database: 'data_storage',
            },
            // ...
        },
        /* client: {
             // host
             host: 'localhost',
             // 端口号
             port: '3306',
             // 用户名
             user: 'root',
             // 密码
             password: '123456',
             // 数据库名
             database: 'research_platform',
         },*/
        // 是否加载到 app 上，默认开启
        app: true,
        // 是否加载到 agent 上，默认关闭
        agent: false,
    };

    // add your middleware config here
    config.middleware = [];

    config.passportLocal = {
        idField: 'id',
        usernameField: 'username',
        passwordField: 'password',
    };

    // add your user config here
    const userConfig = {
        // myAppName: 'egg',
    };

    config.multipart = {
        /*autoFields: true,
        defaultCharset: 'utf8',
        fieldNameSize: 100,
        fieldSize: '100kb',
        fields: 100,
        fileSize: '2048mb',
        files: 100,
        fileExtensions: [],*/
        whitelist: ['.csv'],
    };

    config.listen = {
        port: 7001,
        hostname: '0.0.0.0', // 不建议设置 hostname 为 '0.0.0.0'，它将允许来自外部网络和来源的连接，请在知晓风险的情况下使用
        // path: '/var/run/egg.sock',
    }
    return {
        ...config,
        ...userConfig,
    };
};
