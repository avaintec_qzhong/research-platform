# linkingmed.frontend.react-cli简介

## 脚手架开始时
1. react 16.4.1
2. create-react-app 作为基础脚手架
3. eslint-config-airbnb 作为js规范
4. css-modal modules模式
5. yarn start -开发模式
6. yarn devTest -开发测试模式（测试人员使用）
7. yarn build -产品模式
8. 根目录projectConfig.js 配置开发地址，测试地址，产品地址等产品信息

## 经过放疗世界杯第二场
1. webpack中，关于资源路径，会经过splice函数切割,所以在设置路径是记得后面加上/
`  const publicUrl = publicPath.slice(0, -1);
   /worldcup/.slice(0, -1)，返回/worldcup，
`
2. 安装babel-plugin-import 按需加载
3. 修改webpack.config.dev.js和webpack.config.prod.js 文件，安装引入atd/atdmobile
4. 在env.js中通过
`'GLOBAL_STATUS':process.env.DEVTEST?JSON.stringify('devTest')  
  : (process.env.NODE_ENV==='production'?JSON.stringify('production')
  :JSON.stringify('dev'))
`
5. 给项目全局注入GLOBAL_STATUS变量，方便全局根据不同的环境，加载不同的资源.
 
    |环境 | GLOBAL_STATUS |
    |----- | ----- |
    | 开发环境 | dev |
    | 开发测试环境 | devTest |
    | 产品环境 | production |
  


