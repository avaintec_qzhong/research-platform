import InputLayOut from './layout/InputLayOut';

import Result from './router/AAD/Result';
import Example from './router/example';
import LoginPage from './router/LoginPage';
import DataList from './router/dataList/index.jsx';
import CaseManage from './router/CaseManage/index.jsx';
import ShowData from './router/showData/index.jsx';
import FlatChart from './router/FlatChart/index.jsx';
import OneItem from './router/CaseManage/oneItem/index.jsx';
import ShowModuleInfo from './router/showModuleInfo/index.jsx';


export default [
    // app
    {
        path: '/',
        name: '个人宏观',
        component: InputLayOut,
        child: [
            {
                path: 'DataList',
                name: 'DataList',
                component: DataList,
            },
            {
                path: 'CaseManage',
                name: 'CaseManage',
                component: CaseManage,
            },
            {
                path: 'aadresult',
                name: 'aadresult',
                component: Result,
            },
            {
                path: 'example',
                name: 'example',
                component: Example,
            },
            {
                path: 'ShowData',
                name: 'ShowData',
                component: ShowData,
            },
            {
                path: 'FlatChart',
                name: 'FlatChart',
                component: FlatChart,
            },
            {
                path: 'OneItem',
                name: 'OneItem',
                component: OneItem,
            },
            {
                path: 'ShowModuleInfo',
                name: 'ShowModuleInfo',
                component: ShowModuleInfo,
            }
        ],
    },
    {
        path: '/login',
        name: '登陆',
        component: LoginPage,
        child: [],
    },
];