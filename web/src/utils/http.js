import axios from 'axios'
import {hashHistory} from 'react-router';

// axios.defaults.baseURL = 'http://172.20.107.49:100'; // 配置axios请求的地址
// axios.defaults.baseURL = 'http://172.20.107.49:100'; // 配置axios请求的地址
axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
axios.defaults.crossDomain = true;
axios.defaults.withCredentials = true;  //设置cross跨域 并设置访问权限 允许跨域携带cookie信息
//axios.defaults.headers.common['Authorization'] = ''; // 设置请求头为 Authorization


axios.interceptors.response.use(function (response) {
    // 用户信息是否超时，重定向到登录页面
    if (response.data.state === 3) {
        hashHistory.push('/login')
    }

    return response.data
}, function (error) {
    // Do something with response error
    return Promise.reject(error)
});

function xhr_get(url, params = {}, ok_callbak, err_callback) {
    axios.get(url, {params: params})
        .then(function (response) {
            if (ok_callbak) {
                ok_callbak(response)
            }
        })
        .catch(function (error) {
            if (err_callback) {
                err_callback(error)
            }
        });
}

function xhr_post(url, params, ok_callbak, err_callback) {
    axios.post(url, params)
        .then(function (response) {
            if (ok_callbak) {
                ok_callbak(response)
            }
        })
        .catch(function (error) {
            if (err_callback) {
                err_callback(error)
            }
        });
}

export {xhr_get, xhr_post}


