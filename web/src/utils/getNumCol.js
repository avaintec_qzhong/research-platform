export default () => {
    self.addEventListener('message', function (e) {
        console.log(e.data);
        let result = {};
        for (let key in e.data[0]) {
            if (!isNaN(e.data[0][key])) {
                result[key] = []
            }
        }

        e.data.forEach((dic) => {
            for (let key in dic) {
                if (key in result) {
                    if (!isNaN(dic[key])) {
                        result[key].push(dic[key])
                    } else {
                        result[key].push(null)
                    }
                }

            }
        });
        self.postMessage(result);
    }, false);
}
