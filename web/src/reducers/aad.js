const aadStore = {
    name: '',
    sex: '男',
    chiefComplain: '',
    pastHistory: '',
    Other: '',
};

const aadState = (state = aadStore, action) => {
    switch (action.type) {
        case 'AAD_SAVE_STATE':
            return {...state, ...action.data};
        case 'AAD_RESET_STATE':
            return {...aadStore};
        default:
            return state;
    }
};


export default aadState
