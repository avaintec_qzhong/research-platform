const textStore = {
    lang: 'cn',
    login: false
};

const globalState = (state = textStore, action) => {
    switch (action.type) {
        case 'AAD_CHANGE_LANG':
            return {...state, ...action.data};
        case 'AAD_SAVE_LOGIN':
            return {...state, ...action.data};
        default :
            return state;
    }
};

export default globalState
