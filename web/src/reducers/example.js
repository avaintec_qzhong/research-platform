// reducer
const initialState = {count: 0};


const example = (state = initialState, action) => {
    switch (action.type) {
        case 'EXAMPLE_PLUS_ONE':
            setTimeout(() => {
                console.log('异步返回');
                return {count: state.count + 1};
            }, 1000);
        case 'EXAMPLE_MINUS_ONE':
            return {count: state.count - 1};
        case 'EXAMPLE_CUSTOM_COUNT':
            return {count: state.count + action.payload.count};
        /* default:
             return state;*/
    }
    return state
};


export default example;
