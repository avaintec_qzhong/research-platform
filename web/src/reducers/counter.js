// reducer
const initialState = {count: 0};

const counter = (state = initialState, action) => {
    switch (action.type) {
        case 'PLUS_ONE':
            setTimeout(() =>{
                console.log('异步返回',state.count);
                return {count: state.count + 1};
            }, 1000);
        case 'MINUS_ONE':
            return {count: state.count - 1};
        case 'CUSTOM_COUNT':
            return {count: state.count + action.payload.count};
        /* default:
             return state;*/
    }
    return state
};

export default counter