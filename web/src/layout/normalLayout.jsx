import React from 'react'
import style from './normalLayout.less'
import Menus from '../components/Menus'

import dataJson from './particles.json'

import {Layout, Menu, Icon} from 'antd';

const {Header, Sider, Content} = Layout;

const SubMenu = Menu.SubMenu;

export default class normalLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }


    toggle() {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    componentDidMount() {
        particlesJS('particles', dataJson);
    }

    render() {
        return (
            <Layout className={style.conainer}>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                    <Menus collapsed={this.state.collapsed}/>
                </Sider>
                <Layout>
                    <Header style={{background: '#fff', padding: 0}}>
                        <Icon
                            className={style.trigger}
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle.bind(this)}
                        />
                    </Header>
                    <div id="particles">
                        <Content style={{
                            margin: '12px 16px', padding: 12, minHeight: 280, overflow: 'auto',
                        }}
                        >

                            {this.props.children && React.cloneElement(this.props.children, {
                                showSider: 1
                            })}


                        </Content>
                    </div>
                </Layout>
            </Layout>
        );
    }
}

