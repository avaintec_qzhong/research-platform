import React from 'react'
import style from './InputLayOut.less'
import {Layout, Menu, Breadcrumb, Icon, Select, Card} from 'antd';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {xhr_get} from '../utils/http'
import {hashHistory} from 'react-router';

let bg = require('../assets/img/bg.png');
const {Option} = Select;

const {Header, Content, Footer, Sider} = Layout;

const txt = {
    cn: {title: 'Serral beta0.0.1 辅助诊断平台'},
    en: {title: 'Serral beta0.0.1 Auxiliary Diagnosis Platform'},
    suomi: {title: 'Serral beta0.0.1 Apuvianmäärityksen foorumi'}
};


class InputLayOut extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    onCollapse(collapsed) {
        console.log('-----------', collapsed);
        this.setState({collapsed});
    };

    componentDidMount() {
        /* if (!this.props.globalState.login) {
             this.props.router.push('/login')
         }*/
        // this.cheklogin();
        // particlesJS('particles', dataJson);
    }

    cheklogin() {
        xhr_get('/checklogin', {});
    }

    changeLang(value) {
        this.props.AAD_CHANGE_LANG({lang: value})
    }

    goPath(path) {
        hashHistory.push(path)
    }

    render() {
        console.log(this.props);
        return (
            <Layout className={style.layerContent} style={{minHeight: '100vh'}}>
                <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse.bind(this)}>
                    <div className={style.logo}>
                        <i>D</i>
                        <i>a</i>
                        <i>t</i>
                        <i>a</i>
                        <i>G</i>
                        <i>o</i>
                    </div>
                    <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                        <Menu.Item key="1" onClick={this.goPath.bind(this, '/DataList')}>
                            <Icon type="pie-chart"/>
                            <span>数据集</span>
                        </Menu.Item>
                        <Menu.Item key="2" onClick={this.goPath.bind(this, '/CaseManage')}>
                            <Icon type="desktop"/>
                            <span>项目管理</span>
                        </Menu.Item>
                        {/*<Menu.Item key="4" onClick={this.goPath.bind(this, '/FlatChart')}>*/}
                            {/*<Icon type="file"/>*/}
                            {/*<span>案件管理</span>*/}
                        {/*</Menu.Item>*/}
                        <Menu.Item key="3" onClick={this.goPath.bind(this, '/ShowModuleInfo')}>
                            <Icon type="file"/>
                            <span>算法查看</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{background: '#fff', padding: 0}}>
                        <Icon
                            className="trigger"
                            style={{marginLeft: 20}}
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.onCollapse.bind(this, !this.state.collapsed)}
                        />
                        {/*<Select defaultValue={this.props.globalState.lang} style={{width: 120}}*/}
                        {/*onChange={this.changeLang.bind(this)}>*/}
                        {/*<Option value="cn">中文</Option>*/}
                        {/*<Option value="en">English</Option>*/}
                        {/*<Option value="suomi">Suomi</Option>*/}
                        {/*</Select>*/}
                    </Header>
                    <Content>
                        <Card className={style.mainContent}>
                            {this.props.children}
                        </Card>
                    </Content>
                    {/*<Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>*/}
                </Layout>
            </Layout>
        );
    }
}

const mapSateToPros = (state) => {
    return {...state}
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        AAD_CHANGE_LANG: (data) => dispatch({type: 'AAD_CHANGE_LANG', data: data})
    }, dispatch);
};
export default connect(mapSateToPros, mapDispatchToProps)(InputLayOut)

