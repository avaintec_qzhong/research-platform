import React from 'react';
import ReactDOM from 'react-dom';
import App from './router';
import registerServiceWorker from './registerServiceWorker';
import echarts from 'echarts';
import zhejiang from "./components/Echarts/zhejiang";
import 'normalize.css';
import './global.less'
echarts.registerMap('zhejiang', zhejiang);

ReactDOM.render(<App/>, document.getElementById('root'));

registerServiceWorker();
