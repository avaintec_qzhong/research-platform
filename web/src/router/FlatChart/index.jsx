import React from 'react'
import style from './index.less'
import jsplumb from 'jsplumb'
import {Card, Icon, Select, Checkbox, Row, Col, Menu} from 'antd'

//  [Bezier],[Flowchart],[StateMachine ],[Straight ]

const {Option} = Select;
const {SubMenu} = Menu;
let tools = [
    {
        name: '特征工程',
        child: [
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "分词工具",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "word2vec",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "PCA降维",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
        ]
    },
    {
        name: '机器学习',
        child: [
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "分词工具",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "word2vec",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "PCA降维",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
        ]
    },
    {
        name: '统计分析',
        child: [
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "分词工具",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "word2vec",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "PCA降维",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
        ]
    },
    {
        name: '文本处理',
        child: [
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "分词工具",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "word2vec",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
            {
                "source_path": "absolute addr",
                "target_path": "absolute addr",
                "type": "Remove or Retain",
                "fenci_cols": ["出院诊断", "现病史"],
                "web_args": {
                    "name": "PCA降维",
                    "configs": [{
                        "name": "参数一",
                        "type": "checkbox",
                        "key": "key_1",
                        "option": [1, 2, 3, 4, 5],
                        "value": [1]
                    }, {"name": "参数二", "type": "select", "key": "key_2", "option": [1, 2, 3, 4, 5]}, {
                        "name": "选择列",
                        "type": "select",
                        "key": "key_3",
                        "option": [1, 2, 3, 4, 5]
                    }]
                }
            },
        ]
    },
]

let i = 1;
export default class FlatChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            needSetConfig: {}
        };
        this.graph = {
            name: 'data'
        };
        this.tempDoms = [
            {name: 'source', type: 'source_data', id: 'n0', target: [], cols: ['a', 'b', 'c', 'd', 'f', 'g']},
        ];
    }

    componentDidMount() {
        console.log(jsplumb);
        this.jsPlumbInit()
    }

    __createTools() {
        let that = this;
        return tools.map((obj) => (
            <div draggable={true} onDragStart={that.drag.bind(this, obj)}>
                <Icon type={obj.web_args.icon}/>
                <p>
                    {obj.web_args.name}
                </p>
            </div>
        ))
    }

    createTools() {

        let that = this;
        return (
            <Menu
                size='small'
                defaultOpenKeys={[1]}
                mode="inline"
            >
                {tools.map((item, index) => {
                    return (
                        <SubMenu
                            key={index}
                            title={<span><Icon type="mail"/><span>{item.name}</span></span>}
                        >
                            {item.child.map((obj, index2) => (<Menu.Item
                                draggable={true}
                                key={index + '-' + index2}
                                onDragStart={this.drag.bind(this, obj)}>{obj.web_args.name}</Menu.Item>))}

                        </SubMenu>
                    )
                })}
            </Menu>
        )
    }

    jsPlumbInit() {
        let reactClass = this;
        jsPlumb.ready(function () {
            jsPlumb.importDefaults({
                ConnectionOverlays: [
                    ["Arrow", {location: 0.5}]
                ]
            });
            jsPlumb.setContainer('diagramContainer');

            jsPlumb.addEndpoint('n0', {
                isSource: true,
                isTarget: false,
                maxConnections: -1,
                connector: ["Straight"],
                anchors: 'Right'
            });

            jsPlumb.draggable('n0');
            jsPlumb.bind("connection", function (info, originalEvent) {
                console.log('-------------connection');
                console.log(info.targetEndpoint);
                console.log('-------------connection');
                reactClass.addConnection(info)
            });
            jsPlumb.bind("connectionDrag", function (info, originalEvent) {
                console.log('connectionDrag');
                console.log(info, originalEvent);
            });
            jsPlumb.bind("dblclick", function (conn, originalEvent) {
                //jsPlumb.detach(conn)
                jsPlumb.deleteConnection(conn);
                reactClass.deleteConnection(conn)

            });
        })

    }

    /**
     * 拖拽操作
     * */
    drag(needConfig, event) {
        event.dataTransfer.setData("type", 'tool');
        event.dataTransfer.setData("config", JSON.stringify(needConfig));
    }

    dragOut(event) {
        event.dataTransfer.setData("type", 'out');
    }

    dragJoin(event) {
        event.dataTransfer.setData("type", 'join');
    }

    allowDrop(event) {
        event.preventDefault()
    }

    dragOk(event) {
        event.preventDefault();
        var type = event.dataTransfer.getData("type");
        if (type === 'tool') {
            this.appendTool({
                x: event.clientX,
                y: event.clientY,
                config: JSON.parse(event.dataTransfer.getData("config"))
            })
        } else if (type === 'join') {
            this.appendJoin({
                x: event.clientX,
                y: event.clientY,
                config: JSON.parse(event.dataTransfer.getData("config") || "{}")
            })
        } else {
            this.appendOut({
                x: event.clientX,
                y: event.clientY,
                config: JSON.parse(event.dataTransfer.getData("config") || "{}")
            })
        }
        //alert(data)
    }

    /**
     * jsplumb增加节点，连线等
     * */
    addToolConfig(id) {
        var connectorStyle = {
            //端点样式
            paintStyle: {
                fill: "#7AB02C",
                radius: 7
            },
            /*  //连线类型
              connector: ["StateMachine", {
                  stub: [40, 60],
                  gap: 10,
                  cornerRadius: 5,
                  alwaysRespectStubs: true
              }
              ],
              //连线样式
              connectorStyle: {
                  strokeWidth: 1,
                  stroke: "#9C9DA9",
                  joinstyle: "round",
                  outlineStroke: "none"
              },*/
            //鼠标移上样式
            hoverPaintStyle: {
                fill: "#216477",
                stroke: "#216477"
            }
        };

        jsPlumb.addEndpoint(id, {
            isSource: false,
            isTarget: true,
            connector: ["Straight"],
            anchors: 'Left',

        }, connectorStyle);

        jsPlumb.addEndpoint(id, {
            isSource: true,
            isTarget: false,
            connector: ["Straight"],
            anchors: 'Bottom',

        }, connectorStyle);

        jsPlumb.addEndpoint(id, {
            isSource: true,
            isTarget: false,
            connector: ["Straight"],
            anchors: 'Right',
            maxConnections: -1,
            paintStyle: {
                stroke: "#7AB02C",
                fill: "transparent",
                radius: 7,
                strokeWidth: 1
            }
        }, connectorStyle);

        jsPlumb.draggable(id)
    }

    addJoinConfig(id) {
        var connectorStyle = {
            //端点样式
            paintStyle: {
                fill: "#7AB02C",
                radius: 7
            },
            //连线类型
            connector: ["StateMachine", {
                stub: [40, 60],
                gap: 10,
                cornerRadius: 5,
                alwaysRespectStubs: true
            }
            ],
            //连线样式
            connectorStyle: {
                strokeWidth: 6,
                stroke: "#9C9DA9",
                joinstyle: "round",
                outlineStroke: "none"
            },
            //鼠标移上样式
            hoverPaintStyle: {
                fill: "#216477",
                stroke: "#216477"
            }
        };

        jsPlumb.addEndpoint(id, {
            isSource: false,
            isTarget: true,
            _place: 1,
            connector: ["Straight"],
            anchors: [0, 0.25, 0, 0],
        }, connectorStyle);

        jsPlumb.addEndpoint(id, {
            isSource: false,
            isTarget: true,
            connector: ["Straight"],
            anchors: [0, 0.75, 0, 0],
        }, connectorStyle);

        jsPlumb.addEndpoint(id, {
            isSource: true,
            isTarget: false,
            maxConnections: -1,
            connector: ["Straight"],
            anchors: 'Right',
            paintStyle: {
                stroke: "#7AB02C",
                fill: "transparent",
                radius: 7,
                strokeWidth: 2
            }
        }, connectorStyle);

        jsPlumb.draggable(id)
    }

    addOutConfig(id) {
        var connectorStyle = {
            //端点样式
            paintStyle: {
                fill: "#7AB02C",
                radius: 7
            },
            //连线类型
            connector: ["StateMachine", {
                stub: [40, 60],
                gap: 10,
                cornerRadius: 5,
                alwaysRespectStubs: true
            }
            ],
            //连线样式
            connectorStyle: {
                strokeWidth: 6,
                stroke: "#9C9DA9",
                joinstyle: "round",
                outlineStroke: "none"
            },
            //鼠标移上样式
            hoverPaintStyle: {
                fill: "#216477",
                stroke: "#216477"
            }
        };
        jsPlumb.addEndpoint(id, {
            isSource: false,
            isTarget: true,
            connector: ["Straight"],
            anchors: 'Top',

        }, connectorStyle);
        jsPlumb.draggable(id)
    }

    appendOut(dic) {
        let div = document.createElement('div');
        div.className = 'appendBox';
        div.id = 'n' + i++;
        div.innerHTML = `<div>
                            <span>${div.id}</span>
                            <p>输出</p>
                         </div>`

        div.ondblclick = this.deleteToolBox.bind(this, div);
        let parentDomInfp = this.flatContentDom.getBoundingClientRect();
        div.style.top = dic.y - parentDomInfp.top - 50 + 'px';
        div.style.left = dic.x - parentDomInfp.left - 50 + 'px';
        this.flatContentDom.appendChild(div);
        this.addOutConfig(div.id);
        let nodeDataOrigin = {name: 'outxxx', type: 'out', id: div.id, source: null, config: dic.config};
        this.tempDoms.push(nodeDataOrigin);
        div.onclick = this.setToolconfig.bind(this, nodeDataOrigin);
        div.click();
    }

    appendTool(dic) {
        let div = document.createElement('div');
        div.className = 'appendBox';
        div.id = 'n' + i++;
        div.innerHTML = `<div>
                            <span>${div.id}</span>
                            <p>${dic.config.web_args.name}</p>
                         </div>`
        div.ondblclick = this.deleteToolBox.bind(this, div);
        let parentDomInfp = this.flatContentDom.getBoundingClientRect();
        div.style.top = dic.y - parentDomInfp.top - 45 + 'px';
        div.style.left = dic.x - parentDomInfp.left - 45 + 'px';
        this.flatContentDom.appendChild(div);
        this.addToolConfig(div.id);

        let nodeDataOrigin = {
            name: 'toolxxx',
            type: 'tool',
            id: div.id,
            source: null,
            cols: [],
            target: [],
            config: dic.config
        };
        this.tempDoms.push(nodeDataOrigin);
        div.onclick = this.setToolconfig.bind(this, nodeDataOrigin);
        div.click();
    }

    appendJoin(dic) {
        let div = document.createElement('div');
        div.className = 'appendBox';
        div.id = 'n' + i++;
        // div.innerText = "拼接";

        div.innerHTML = `<div>
                            <span>${div.id}</span>
                            <p>拼接</p>
                         </div>`

        div.ondblclick = this.deleteToolBox.bind(this, div);

        let parentDomInfp = this.flatContentDom.getBoundingClientRect();
        div.style.top = dic.y - parentDomInfp.top - 45 + 'px';
        div.style.left = dic.x - parentDomInfp.left - 45 + 'px';
        this.flatContentDom.appendChild(div);
        this.addJoinConfig(div.id);
        let nodeDataOrigin = {
            name: 'join',
            type: 'join',
            id: div.id,
            cols: [],
            _cols: {},
            source: [],
            target: [],
            config: dic.config
        }
        this.tempDoms.push(nodeDataOrigin);
        div.onclick = this.setToolconfig.bind(this, nodeDataOrigin);
        div.click();
    }

    addConnection(info) {
        /***
         * info.source.id连接线的源头
         * info.target.id连接线的目的地
         * */
        this.tempDoms.forEach((obj) => {
            switch (obj.type) {
                case "source_data":
                    if (obj.id === info.sourceId) {
                        obj.target.push(info.targetId)
                    }
                    break;
                case "join":
                    if (obj.id === info.targetId) {
                        obj.source.push(info.sourceId)
                    }

                    if (obj.id === info.sourceId) {
                        obj.target.push(info.targetId)
                    }
                    break;
                case "tool":
                    if (obj.id === info.sourceId) {
                        obj.target.push(info.targetId)
                    }
                    if (obj.id === info.targetId) {
                        obj.source = info.sourceId
                    }
                    break;
                case  "out":
                    if (obj.id === info.targetId) {
                        obj.source = info.sourceId
                    }
                    break
            }
        });
        this.resetPathCols(info);
        console.log('---addConnection---');
        console.log(this.tempDoms)
    }

    /**
     * jsplumb删除节点，删除连线等
     * */

    /**
     * 连接线路后，列在算法之间的默认传递
     * */
    resetPathCols(info) {
        let that = this;

        function derectorItemCols(fatherNode, tempDoms) {
            tempDoms.forEach((item, index, origin_tempDoms) => {
                switch (item.type) {
                    case "source_data":
                        break;
                    case "join":
                        if (item.source.indexOf(fatherNode.id) !== -1) {
                            let cols = [];
                            if (fatherNode.choose_cols) {
                                cols = fatherNode.choose_cols.map((v) => (v + '>' + item.id))
                            } else {
                                cols = fatherNode.cols.map((v) => (v + '>' + item.id))
                            }
                            item._cols[fatherNode.id + '_cols'] = cols;
                            item.cols = []
                            for (let key in item._cols) {
                                item.cols = [...item.cols, ...item._cols[key]]
                            }
                            item.chooseCols = [...item.cols]
                            derectorItemCols(item, origin_tempDoms)
                        }
                        break;
                    case "tool":
                        if (item.source === fatherNode.id) {
                            let cols = [];
                            console.log(fatherNode)
                            if (fatherNode.choose_cols) {
                                cols = fatherNode.choose_cols.map((v) => (v + '>' + item.id))
                            } else {
                                cols = fatherNode.cols.map((v) => (v + '>' + item.id))
                            }
                            item.cols = cols;
                            item.chooseCols = [...item.cols]
                            derectorItemCols(item, origin_tempDoms)
                        }
                        break;
                    case  "out":
                        break;
                }
            });
        }

        let fatherNode = this.tempDoms.find((item) => (item.id === info.sourceId));
        derectorItemCols(fatherNode, that.tempDoms)
    }


    deleteConnection(info) {
        /***
         * info.sourceId连接线的源头
         * info.targetId连接线的目的地
         * */
        this.tempDoms.forEach((item) => {
            switch (item.type) {
                case "source_data":
                    item.target.forEach((id, index, targets) => {
                        if (id === info.sourceId) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case "join":
                    item.target.forEach((id, index, targets) => {
                        if (id === info.targetId) {
                            targets.splice(index, 1);
                        }
                    });
                    item.source.forEach((id, index, targets) => {
                        if (id === info.sourceId) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case "tool":
                    if (item.source === info.sourceId) {
                        item.source = null
                    }
                    item.target.forEach((id, index, targets) => {
                        if (id === info.targetId) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case  "out":
                    if (item.source === info.sourceId) {
                        item.source = null
                    }
                    break;
            }

        });

        this.deleteConectionCols(info)
        console.log(info)
    }

    deleteToolBox(div) {
        console.log(div.id);
        jsPlumb.remove(div.id);
        this.deleteToolCols(div.id);
        this.tempDoms.forEach((item, index, tempDoms) => {
            if (item.id === div.id) {
                tempDoms.splice(index, 1);
            }
        });
        this.tempDoms.forEach((item, index, tempDoms) => {
            switch (item.type) {
                case "source_data":
                    item.target.forEach((id, index, targets) => {
                        if (id === div.id) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case "join":
                    item.target.forEach((id, index, targets) => {
                        if (id === div.id) {
                            targets.splice(index, 1);
                        }
                    });
                    item.source.forEach((id, index, targets) => {
                        if (id === div.id) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case "tool":
                    if (item.source === div.id) {
                        item.source = null
                    }
                    item.target.forEach((id, index, targets) => {
                        if (id === div.id) {
                            targets.splice(index, 1);
                        }
                    });
                    break;
                case  "put":
                    if (item.source === div.id) {
                        item.source = null
                    }
                    break;
            }
        });
        console.log(this.tempDoms)
    }

    /**
     * 删除一个连接后，列的重新传递
     * */
    deleteConectionCols(info) {
        let that = this;

        function derectorItemCols(thisNode, fatherNode, tempDoms) {
            tempDoms.forEach((item, index, origin_tempDoms) => {
                switch (item.type) {
                    case "source_data":
                        break;
                    case "join":
                        if (item.id === thisNode.id) {
                            console.log('------join重置------')
                            if (fatherNode.id + '_cols' in item._cols) {
                                delete item._cols[fatherNode.id + '_cols']
                            }
                            item.cols = [];
                            for (let key in item._cols) {
                                item.cols = [...item.cols, ...item._cols[key]]
                            }
                            item.chooseCols = [...item.cols]
                            item.target.forEach((id) => {
                                let _thisNode = origin_tempDoms.find((_item) => (_item.id === id));
                                derectorItemCols(_thisNode, thisNode, origin_tempDoms)
                            });

                        }
                        break;
                    case "tool":
                        if (item.id === thisNode.id) {
                            item.cols = [];
                            console.log('------tool重置------');
                            item.chooseCols = [];
                            item.target.forEach((id) => {
                                let _thisNode = origin_tempDoms.find((_item) => (_item.id === id));
                                derectorItemCols(_thisNode, thisNode, origin_tempDoms)
                            });
                        }
                        break;
                    case  "out":
                        break;
                }
            });
        }

        let thisNode = this.tempDoms.find((item) => (item.id === info.targetId));
        let fatherNode = this.tempDoms.find((item) => (item.id === info.sourceId));
        derectorItemCols(thisNode, fatherNode, that.tempDoms)
    }

    /**
     * 删除一个节点后，列的重新传递
     * */
    deleteToolCols(id) {
        let that = this;
        let thisNode = this.tempDoms.find((item) => (item.id === id));

        function derectorItemCols(fatherNode, tempDoms) {
            fatherNode.target.forEach((id, index, origin_tempDoms) => {
                let item = tempDoms.find((item) => (item.id === id));
                switch (item.type) {
                    case "source_data":
                        break;
                    case "join":
                        console.log('------deleteToolCols_join重置------');
                        if (fatherNode.id + '_cols' in item._cols) {
                            delete item._cols[fatherNode.id + '_cols']
                        }
                        item.cols = [];
                        for (let key in item._cols) {
                            item.cols = [...item.cols, ...item._cols[key]]
                        }
                        item.chooseCols = [...item.cols];
                        item.target.forEach((id) => {
                            let _thisNode = origin_tempDoms.find((_item) => (_item.id === id));
                            derectorItemCols(_thisNode, origin_tempDoms)
                        });
                        break;
                    case "tool":
                        item.cols = [];
                        console.log('------deleteToolCols_tool重置------');
                        item.chooseCols = [];
                        item.target.forEach((id) => {
                            let _thisNode = origin_tempDoms.find((_item) => (_item.id === id));
                            derectorItemCols(_thisNode, thisNode, origin_tempDoms)
                        });
                        break;
                    case  "out":
                        break;
                }
            });
        }

        derectorItemCols(thisNode, this.tempDoms)
    }

    getJson() {
        console.log(this.tempDoms);
        console.log(JSON.stringify(this.tempDoms));
        return
        let that = this;
        this.graphjSON = {
            name: this.tempDoms[0].name,
            type: 'data',
            id: 'source_data',
            child: [],
        };

        function derector(originItem) {
            let get_child = that.tempDoms.find((item) => {
                if (item.source === originItem.id) {
                    derector(item);
                    return true
                }
            });
            originItem.child = get_child;
            return originItem
        }

        function getGraphy(data) {

        }

        this.graphjSON.child = this.tempDoms[0].target.map((one_target) => {

            let thisPath = that.tempDoms.find((item) => {
                return item.id === one_target
            });

            // if(thisPath.type==='join'){
            //     this.graphjSON.joinModule
            // }

            return derector({
                name: thisPath.name,
                type: thisPath.type,
                id: thisPath.id,
                config: thisPath.config,
                child: []
            })
        });
        console.log(this.graphjSON);
        console.log(JSON.stringify(this.graphjSON));

    }

    /*
    * 工具的参数配置
    * */
    setToolconfig(dic) {
        console.log(dic)
        this.setState({
            needSetConfig: dic
        })

    }

    showConfig() {
        console.log(this.state.needSetConfig);
        if (!this.state.needSetConfig.config) {
            return
        } else {
            return (
                <Card>
                    {
                        this.state.needSetConfig.cols && <Row>
                            <Col span={6}>输出列：</Col>
                            <Col span={18}>
                                <Checkbox.Group style={{width: '100%'}} onChange={(value) => {
                                    this.state.needSetConfig.chooseCols = value;
                                    this.forceUpdate()
                                }} value={this.state.needSetConfig.chooseCols}>
                                    {this.state.needSetConfig.cols.map((val) => (
                                        <Checkbox value={val}>{val}</Checkbox>))}
                                </Checkbox.Group>
                            </Col>
                        </Row>
                    }

                    {
                        this.state.needSetConfig.config.web_args && this.state.needSetConfig.config.web_args.configs.map((oneConfig) => {
                            switch (oneConfig.type.toLowerCase()) {
                                case 'select':
                                    return (<Row>
                                        <Col span={6}>{oneConfig.name}</Col>
                                        <Col span={18}>
                                            <Select style={{width: '100%'}}>
                                                {oneConfig.option.map((val) => (
                                                    <Option value={val}>value={val}</Option>))}
                                            </Select>
                                        </Col>
                                    </Row>);
                                    break;
                                case 'checkbox':
                                    return (
                                        <Row>
                                            <Col span={6}>{oneConfig.name}</Col>
                                            <Col span={18}>
                                                <Checkbox.Group style={{width: '100%'}}>
                                                    {oneConfig.option.map((val) => (
                                                        <Checkbox value={val}>value={val}</Checkbox>))}
                                                </Checkbox.Group>
                                            </Col>
                                        </Row>)
                            }
                        })
                    }
                </Card>
            )
        }
    }

    titleCase(str) {
        var newarr, newarr1 = [];
        newarr = str.toLowerCase().split(" ");
        for (var i = 0; i < newarr.length; i++) {
            newarr1.push(newarr[i][0].toUpperCase() + newarr[i].substring(1));
        }
        return newarr1.join(' ');
    }

    render() {
        return (
            <Card
                title={'工作流设计'}
                style={{height: '100%', overflow: 'hidden'}} size='small'>
                {/*<button onClick={this.getJson.bind(this)}>jsonget</button>*/}
                <div className={style.flatChart}>
                    <div className={style.tools}>
                        {this.createTools()}
                        <div draggable={true} style={{height:"40px",lineHeight:"40px", fontSize: 15}}
                             onDragStart={this.dragJoin.bind(this)}><Icon style={{margin: '0 10px 0 5px '}}
                                                                          type="border-horizontal"/>拼接
                        </div>
                        <div draggable={true} style={{height:"40px",lineHeight:"40px", fontSize: 15}}
                             onDragStart={this.dragOut.bind(this)}><Icon style={{margin: '0 10px 0 5px '}}
                                                                         type="logout"/>输出源
                        </div>
                        {/*<div className={style.mathModle}>*/}
                        {/*{this.createTools()}*/}
                        {/*</div>*/}
                        {/*<div className={style.outModel}>*/}
                        {/*<div draggable={true} onDragStart={this.dragJoin.bind(this)}>拼接</div>*/}
                        {/*</div>*/}
                        {/*<div className={style.outModel}>*/}
                        {/*<div draggable={true} onDragStart={this.dragOut.bind(this)}>输出源</div>*/}
                        {/*</div>*/}
                    </div>
                    <div className={style.flatContent}
                         onDragOver={this.allowDrop.bind(this)}
                         onDrop={this.dragOk.bind(this)}
                         ref={(element) => {
                             this.flatContentDom = element
                         }}
                         id='diagramContainer'
                    >
                        <div id='n0' className='appendBox data'>数据源</div>
                    </div>
                    <div className={style.info}>
                        {this.showConfig()}
                    </div>
                </div>
            </Card>
        )
    }

    componentWillUnmount() {
        this.flatContentDom.innerHTML = null
        this.flatContentDom.parentNode.removeChild(this.flatContentDom);
    }
}








