import React from 'react'
import {Table, Divider, Tag, Icon, Row, Radio, Select} from 'antd';
import GeneralCard from '../../../components/generalCard/index';
import style from './index.less'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Tags',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <span>{tags.map(tag => {
                let color = tag.length > 5 ? 'geekblue' : 'green';
                if (tag === 'loser') {
                    color = 'volcano';
                }
                return (
                    <Tag color={color} key={tag}>
                        {tag.toUpperCase()}
                    </Tag>
                );
            })}</span>
        ),
    },
    {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
            <span>
        <a>Invite {record.name}</a>
        <Divider type="vertical"/>
        <a>Delete</a>
      </span>
        ),
    },
];

const data = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];


export default class DataList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showTable: false,
            sortType: 1
        }
    }


    componentDidMount() {

    }

    handleSizeChange(e) {
        this.setState({showTable: e.target.value});
    };

    handleSortChange(e) {
        this.setState({sortType: e.target.value});
    };

    render() {
        return (
            <div>
                <div className={style.filter}>
                    <Radio.Group
                        value={this.state.showTable}
                        onChange={this.handleSizeChange.bind(this)}>
                        <Radio.Button value={true}>表格</Radio.Button>
                        <Radio.Button value={false}> 卡片</Radio.Button>
                    </Radio.Group>

                    <Radio.Group
                        style={{marginLeft: 40}}
                        value={this.state.sortType}
                        onChange={this.handleSortChange.bind(this)}>
                        <Radio.Button value={1}>最新的</Radio.Button>
                        <Radio.Button value={2}> 最老地</Radio.Button>
                        <Radio.Button value={3}> 按字母排序</Radio.Button>
                        <Radio.Button value={4}> 按字母倒序</Radio.Button>
                    </Radio.Group>


                </div>
                {
                    this.state.showTable ? <Table columns={columns} dataSource={data}/> : <Row gutter={16}>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                        <GeneralCard/>
                    </Row>
                }

            </div>)
    }
}


