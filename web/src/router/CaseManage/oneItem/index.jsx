import React from 'react'
import {Tabs, Row, Icon} from 'antd';
//import OriginData from './OriginData/index.jsx'
import ListFlat from './listFlat/index'
import OriginData from '../../dataList/OriginData/index.jsx'
const {TabPane} = Tabs;


export default class DataList extends React.Component {
    componentDidMount() {

    }


    render() {
        return (<div>
            <Tabs onChange={() => {
            }} type="card">
                <TabPane tab="工作流程集" key="1">
                    <ListFlat/>
                </TabPane>
                <TabPane tab="数据集" key="2">
                    <OriginData/>
                </TabPane>
                <TabPane tab="模型集" key="3">
                    1模型集
                </TabPane>
                <TabPane tab="结果集" key="4">
                    <OriginData/>
                </TabPane>
            </Tabs>
        </div>)
    }
}


