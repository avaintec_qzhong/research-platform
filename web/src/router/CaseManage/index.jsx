import React from 'react'
import {Divider, Tag, Icon, Row, Radio, Select} from 'antd';
import CaseCard from '../../components/caseCard/index';
import style from './index.less'


export default class DataList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sortType: 1
        }
    }

    componentDidMount() {

    }


    handleSortChange(e) {
        this.setState({sortType: e.target.value});
    };

    render() {
        return (<div>
            <div className={style.filter}>
                <Radio.Group
                    value={this.state.sortType}
                    onChange={this.handleSortChange.bind(this)}>
                    <Radio.Button value={1}>最新的</Radio.Button>
                    <Radio.Button value={2}> 最老地</Radio.Button>
                    <Radio.Button value={3}> 按字母排序</Radio.Button>
                    <Radio.Button value={4}> 按字母倒序</Radio.Button>
                </Radio.Group>
            </div>
            <Row gutter={8}>
                <CaseCard/>
                <CaseCard/>
                <CaseCard/>
                <CaseCard/>
                <CaseCard/>
            </Row>
        </div>)
    }
}


