import React from 'react'
import {Tabs, Card, Icon, Steps} from 'antd';
import {hashHistory} from 'react-router';
import InputForm from '../input/index.jsx'
import SuBmit from '../SuBmit/index.jsx'
import Result from '../Result/index.jsx'
import Calculation from '../Calculation/index.jsx'

const {Step} = Steps;
export default class Diagnosis extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0
        }
    }

    componentDidMount() {
        console.log(1)

    }

    stepGo(num) {
        console.log('llll')
        this.setState({
            step: num
        }, () => {
            console.log(this.state.step)
        })
    }


    goback() {
        hashHistory.goBack();
    }

    render() {
        return (
            <div style={{margin: '0px 50px'}}>
                <div style={{width: 550, margin: '0 auto'}}>
                    <Steps current={this.state.step} size='small'>
                        <Step title="病情输入" description=""/>
                        {this.state.step === 1 &&
                        <Step status="wait" title="计算中" icon={<Icon style={{color: '#1890ff'}} type="loading"/>}/>}

                        <Step title="诊断结果" description=""/>
                    </Steps>
                </div>
                {this.state.step === 0 && <InputForm
                    stepGo={this.stepGo.bind(this)}
                />}
                {this.state.step === 1 && <Calculation
                    stepGo={this.stepGo.bind(this)}
                />}
                {this.state.step === 2 && <Result
                    stepGo={this.stepGo.bind(this)}
                />}

            </div>
        )
    }
}