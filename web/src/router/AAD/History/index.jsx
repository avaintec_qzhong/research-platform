import React from 'react'
import {Table, Icon, Tooltip} from 'antd';
import {xhr_get} from "../../../utils/http";

const columns = [
    {
        title: '序号',
        dataIndex: 'name',
        render: (_, obj, num) => (<p key={num}>{num}</p>)
    },
    {
        title: '患者名',
        dataIndex: 'name',
    },
    {
        title: '年龄',
        dataIndex: 'age',
    },
    {
        title: '预测情况',
        dataIndex: 'pre_organ',
    },
    {
        title: '实际情况',
        dataIndex: 'actual_organ',
    },
    {
        title: '反馈评价',
        dataIndex: 'feedback',
    },
    {
        title: '操作',
        dataIndex: 'name',
        render: (_, obj, num) => (
            <p key={num} style={{fontSize: 18}}>
                <Tooltip placement="top" title='查看具体信息'>
                    <Icon type="edit"/>
                </Tooltip>
                <Tooltip placement="top" title='提交反馈'>
                    <Icon style={{color: 'red', marginLeft: 15}} type="exclamation-circle"/>
                </Tooltip>
            </p>)
    },
];


export default class HistoryTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        this.getdata()
    }

    getdata() {
        xhr_get('/aad/getdata', {}, (response) => {
            this.setState({
                data: response.result
            })
        })
    }

    render() {
        return (
            <div>

                <Table columns={columns} dataSource={this.state.data} size="small"/>
            </div>
        )
    }

}