import React from 'react'
import {Progress} from 'antd';
import style from './index.less'

import {connect} from 'react-redux'

class Calculation extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manageData: 0,
            caculation: 0
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.caculation > 50 && nextProps.aadState.pre_organ) {
            this.props.stepGo(2)
        }
    }


    componentDidMount() {
        let manageDataTime = setInterval(() => {
            this.setState({
                manageData: this.state.manageData + 10
            }, () => {
                if (this.state.manageData === 100) {
                    clearInterval(manageDataTime);
                    let caculationTime = setInterval(() => {
                        this.setState({
                            caculation: this.state.caculation + 5
                        }, () => {
                            if (this.state.caculation > 90) {
                                clearInterval(caculationTime)
                            }
                        })
                    }, 100)
                }
            })
        }, 200)
    }

    render() {
        if (this.state.caculation > 50 && this.props.aadState.pre_organ) {
            this.props.stepGo(2)
        }
        return (
            <div className={style.calculation}>
                <Progress type="circle" percent={this.state.manageData}
                          format={() => ('数据整理' + this.state.manageData + '%')}/>
                <Progress type="circle" percent={this.state.caculation}
                          format={() => ('模型运算' + this.state.caculation + '%')}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {...state}
};


export default connect(mapStateToProps)(Calculation)
