import React from 'react'
import Echarts from '../../../../../components/Echarts'
import Style from './index.less'
import PreResult from "../../index";

let types = {
    '胃': {
        name: ['急性胃炎', '十二指肠溃疡', '慢性咽炎', '胃溃疡'],
        value: [90, 80, 60, 40]
    },
    '肠': {
        name: ['急性肠炎', '慢性肠炎', '肠穿孔', '肠粘接'],
        value: [80, 70, 20, 10]
    },
    '肝': {
        name: ['肝炎', '脂肪肝', '酒精肝', '肝癌症'],
        value: [120, 60, 50, 40]
    },
    '胆囊': {
        name: ['胆囊炎', '胆囊穿孔', '胆囊解释', '胆囊肥大'],
        value: [90, 80, 60, 40]
    },
    '胰': {
        name: ['急性胃炎', '十二指肠溃疡', '慢性咽炎', '胃溃疡'],
        value: [90, 80, 60, 40]
    },
    '肾': {
        name: ['肾结石', '肾炎'],
        value: [90, 80]
    },
    '输尿管': {
        name: ['输尿管结石', '输尿管阻塞'],
        value: [90, 80]
    },
    '膀胱': {
        name: ['膀胱炎', '膀胱癌症', '膀胱结石',],
        value: [90, 80, 60, 40]
    },
    '甲状腺': {
        name: ['甲状腺炎', '甲状腺肥大', '甲状腺功能丧失'],
        value: [90, 80, 60]
    },
    '心脏': {
        name: ['心脏病', '心肌肥大', '心律失衡', '心律功能'],
        value: [90, 80, 60, 40]
    },
    '淋巴': {
        name: ['淋巴癌症', '淋巴炎症', '免疫力问题', '淋巴瘤'],
        value: [190, 80, 60, 40]
    },
    '胸腺': {
        name: ['急性胃炎', '十二指肠溃疡', '慢性咽炎', '胃溃疡'],
        value: [90, 80, 60, 40]
    },
    '脾': {
        name: ['胸腺瘤', '胸腺脂肪瘤', '恶性淋巴瘤', '精原细胞瘤'],
        value: [90, 80, 60, 40]
    },
    '阑尾': {
        name: ['急性阑尾炎', '慢性阑尾炎', '阑尾病变', '阑尾穿孔'],
        value: [190, 80, 60, 40]
    }
};
export default class Pie extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pre_organ_max_be: [],
        }
    }

    convertData(data) {
        let res = [
            {
                name: '杭州市',
                value: [120.056132, 30.313681, 1000],
                info: '随便写的'
            }
        ];
        for (let i = 0; i < data.length; i++) {
            let geoCoord = mapData[data[i].locName];
            if (geoCoord) {
                res.push({
                    name: data[i].locName,
                    value: geoCoord.concat(data[i].count),
                    info: data[i].povertyDisLoan
                });
            }
        }
        return res;
    }

    getOption() {
        let pre_organ = this.props.pre_organ;
        let pre_organ_score = this.props.pre_organ_score;
        let pre_organ_pie = [];
        let pre_organ_pie_lable = [];
        pre_organ.forEach((organ, idenx) => {
            if (pre_organ_score[idenx] > 2) {
                let temp = {};
                temp.name = organ;
                temp.value = pre_organ_score[idenx];
                pre_organ_pie.push(temp);
                pre_organ_pie_lable.push(organ);
            }
        });
        let pre_organ_max_name = pre_organ[pre_organ_score.indexOf(Math.max(...pre_organ_score))];
        let SymptomNameValue = types[pre_organ_max_name];

        let option = {
            /* title: {
                 text: 'serral预测结果',
                 x: 'center'
             },*/
            grid: [{top: '55%', width: '80%', height: '40%'}],
            legend: {
                orient: 'vertical',
                right: '100',
                itemWidth: 20,
                itemHeight: 10,
                //data: ['肾', '肝脏', '胰腺']
                data: pre_organ_pie_lable
            },
            xAxis: [
                {
                    type: 'category',
                    //data: ['肾结石', '肾炎', '尿毒症', '慢性肾炎', '肾下垂'],
                    data: SymptomNameValue.name,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    type: 'pie',
                    radius: '35%',
                    center: ['50%', '27%'],
                    /*data: [
                        {value: 90, name: '肾'},
                        {value: 20, name: '肝脏'},
                        {value: 80, name: '胰腺'}
                    ],*/
                    data: pre_organ_pie,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    "color": [
                        "#ff507a",
                        "#e6b600",
                        "#0098d9",
                        "#2b821d",
                        "#005eaa",
                        "#339ca8",
                        "#cda819",
                        "#32a487"
                    ],
                },
                {
                    type: 'bar',
                    //data: [80, 70, 60, 50, 49],
                    data: SymptomNameValue.value,
                    center: '55%',
                    "color": [
                        "#3fb1e3",
                        "#6be6c1",
                        "#626c91",
                        "#a0a7e6",
                        "#c4ebad",
                        "#96dee8"
                    ],
                },

            ]
        };
        return option;
    }

    getOrgan() {
        let pre_organ = this.props.pre_organ;
        let pre_organ_score = this.props.pre_organ_score;
        let pre_organ_max_be = [];
        pre_organ.forEach((organ, idenx) => {
            if (pre_organ_score[idenx] > 3.5) {
                pre_organ_max_be.push(organ)
            }
        });
        return pre_organ_max_be
    }

    get_pre_organ_max_name() {
        let pre_organ = this.props.pre_organ;
        let pre_organ_score = this.props.pre_organ_score;
        let pre_organ_max_name = pre_organ[pre_organ_score.indexOf(Math.max(...pre_organ_score))];
        return types[pre_organ_max_name].name[0];
    }

    render() {
        return (
            <div>
                <div className={Style.result}>
                    <p>模型通过学习以历史数据，根据目前患者的输入信息综合分析认为：</p>
                    <p>最有可能是
                        <span className={Style.result_title}>{this.getOrgan().map((organ) => (organ + ','))}</span>
                        器官问题导致患者病情，具体病因可能是
                        <span className={Style.result_title}>{this.get_pre_organ_max_name()}</span></p>
                    <p>模型具体预测情况如下：</p>
                </div>

                <Echarts
                    option={this.getOption()}
                    style={{width: '100%', height: '400px'}}
                />
            </div>
        )
    }

}


