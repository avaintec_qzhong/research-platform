import React from 'react'


import {Collapse, Card} from 'antd';

const {Panel} = Collapse;

export default class Treatment extends React.Component {
    render() {
        return (
            <Card title="治疗建议" bordered={true} style={{paddingBottom: 20}}>
                <Collapse defaultActiveKey={['1']}>
                    <Panel header="肾结石治疗建议：" key="1">
                        <p>一般情况下，结石在0.6cm以下的是可以使用药物排石,常用的药物如肾石通颗粒,排石颗粒等.大于0.6cm以上的是体外冲击波碎石,大于2.0cm以上的可以手术治疗</p>
                    </Panel>
                    <Panel header="肾炎治疗建议" key="2">
                        <p>1.卧床休息、饮食控制、利尿消肿、抗感染、降压等，其中饮食控制是一个重要部分。急性期：宜酌情限制盐、水、蛋白质的摄入。2.对于高血压、水肿患儿，宜采取低盐甚至无盐饮食，当尿量增多，水肿逐步消退，可给少盐2～3g/日的饮食。3.对于水肿重且尿少者，应适当限制水分的摄入对于合并肾功能减退，出现氮质血症的，应限制蛋白质的摄入，短期内应用优质蛋白，注意以糖类等等提供充分的能量。</p>
                    </Panel>
                </Collapse>
            </Card>

        )
    }

}