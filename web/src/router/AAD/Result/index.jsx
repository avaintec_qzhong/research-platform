import React from 'react';
import {Card, Row, Col, Button} from 'antd';
import style from './index.less'
import PatientCard from './PatientCard'
import PreResult from './PreResult'
import Treatment from './Treatment'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'

class AadResult extends React.Component {
    diagnosisAgain() {
        this.props.stepGo(0)
    }

    render() {
        return (
            <div className={style.AadResultcard}>
                <Row gutter={8}>
                    <Col span={6}>
                        <PatientCard
                            name={this.props.aadState.name}
                            sex={this.props.aadState.sex}
                            chiefComplain={this.props.aadState.chiefComplain}
                            pastHistory={this.props.aadState.pastHistory}
                            Other={this.props.aadState.Other}
                        />
                    </Col>
                    <Col span={12}>
                        <PreResult
                            pre_organ={this.props.aadState.pre_organ}
                            pre_organ_score={this.props.aadState.pre_organ_score}
                        />
                    </Col>
                    <Col span={6}>
                        <Treatment/>
                    </Col>
                </Row>
                <Row gutter={8}>
                    <Col offset={6}>
                        <div style={{marginTop: 25}}>
                            < Button type="primary"
                                     onClick={this.diagnosisAgain.bind(this)}>发起新的诊断</Button>
                        </div>
                    </Col>
                </Row>
            </div>)
    }
}

const mapStateToProps = (state) => {
    return {...state}
};


export default connect(mapStateToProps)(AadResult)