import React from 'react';
import style from './index.less'
import {Card} from 'antd';
import Pie from '../Echarts/pie/Pie'

export default class PreResult extends React.Component {
    render() {
        return (
            <Card  className={style.PreResult} title="预测结果" >
                <Pie
                    pre_organ={this.props.pre_organ}
                    pre_organ_score={this.props.pre_organ_score}
                />
            </Card>
        )
    }
}