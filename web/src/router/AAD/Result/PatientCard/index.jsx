import React from 'react';
import style from './index.less'
import {Card} from 'antd';

export default class PatientCard extends React.Component {

    render() {


        return (
            <Card className={style.PatientCard} title="患者情况" bordered={true} style={this.props.style}>
                <p>
                    姓名：<span >{this.props.name}</span>
                    性别：<span>{this.props.sex}</span>
                </p>
                <div className={style.oneContent}>
                    <div className={style.title}>主诉:</div>
                    <div className={style.txt}>{this.props.chiefComplain}</div>
                </div>
                <div className={style.oneContent}>
                    <div className={style.title}>既往史:</div>
                    <div
                        className={style.txt}>{this.props.pastHistory}</div>
                </div>
                <div className={style.oneContent}>
                    <div className={style.title}>其他:</div>
                    <div className={style.txt}>{this.props.Other}</div>
                </div>
            </Card>
        )
    }
}