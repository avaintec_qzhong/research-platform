import React from 'react'
import {Tabs, Card, Icon, Steps} from 'antd';
import Diagnosis from './diagnosis/index.jsx'
import HistoryTable from './History/index.jsx'
import {hashHistory} from 'react-router';
import FixedTitleCard from '../../components/FixedTitleCard';
import style from './AAD.less'

const {Step} = Steps;
const TabPane = Tabs.TabPane;

//style={{backgroundColor: 'transparent'}}


export default class AAD extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 0,
        }
    }

    componentDidMount() {


    }

    goback() {
        hashHistory.goBack();
    }

    change(num) {
        console.log('------', num)
        this.setState({
            activeTab: num
        })
    }

    render() {
        return (
            <FixedTitleCard title='急腹症诊断' close={this.goback.bind(this)} >
                <Tabs type="card" onChange={this.change.bind(this)}>
                    <TabPane tab="辅助诊断" key="1">
                        <Diagnosis/>
                    </TabPane>
                    <TabPane tab="历史诊断" key="2">
                        {this.state.activeTab == 2 && <HistoryTable/>}
                    </TabPane>
                </Tabs>
            </FixedTitleCard>
        )
    }
}


