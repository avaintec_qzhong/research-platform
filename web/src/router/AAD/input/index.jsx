import {
    Form, DatePicker, TimePicker, Button, Input, Select, Row, Col, Modal, Radio, Switch, Checkbox, Card, InputNumber
} from 'antd';

import {xhr_get, xhr_post} from '../../../utils/http'

const CheckboxGroup = Checkbox.Group;
import React from 'react';
import style from './index.less'
import PatientCard from '../Result/PatientCard'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


const confirm = Modal.confirm;
const {TextArea} = Input;

const {MonthPicker, RangePicker} = DatePicker;


class InputForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            IntelligentFilling: false
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }
            // Should format date value before submit.
            const rangeValue = fieldsValue['range-picker'];
            const rangeTimeValue = fieldsValue['range-time-picker'];
            const values = {
                ...fieldsValue,
                'date-picker': fieldsValue['date-picker'].format('YYYY-MM-DD'),
                'date-time-picker': fieldsValue['date-time-picker'].format('YYYY-MM-DD HH:mm:ss'),
                'month-picker': fieldsValue['month-picker'].format('YYYY-MM'),
                'range-picker': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
                'range-time-picker': [
                    rangeTimeValue[0].format('YYYY-MM-DD HH:mm:ss'),
                    rangeTimeValue[1].format('YYYY-MM-DD HH:mm:ss'),
                ],
                'time-picker': fieldsValue['time-picker'].format('HH:mm:ss'),
            };
            console.log('Received values of form: ', values);
        });
    }


    showConfirm() {
        /* if (this.state.IntelligentFilling) {

         }*/

        this.props.form.validateFields(err => {
            if (!err) {
                // console.info('success');
                let _this = this;
                let params = {
                    name: _this.props.form.getFieldValue('name'),
                    sex: _this.props.form.getFieldValue('sex'),
                    chiefComplain: _this.getchiefComplain(),
                    pastHistory: _this.getpastHistory(),
                    Other: _this.getOther()
                }
                confirm({
                    title: '即将发起诊断',
                    content: '请确认是否填写完毕',
                    onOk() {
                        xhr_post('/aad/pre', params, (response) => {
                            params.pre_organ = response.result.pre_organ;
                            params.pre_organ_score = response.result.pre_organ_score;
                            _this.props.AAD_SAVE_STATE(params);
                            console.log('---------------------response----------------------')
                            console.log(response)
                            console.log(params)
                            _this.props.stepGo(1)
                        });

                    },
                    onCancel() {
                        console.log('Cancel');
                    }
                    ,
                    cancelText: '再确认一下',
                    okText:
                        '发起诊断',

                });
            }
        });
    }

    changeFilling() {
        this.setState({IntelligentFilling: !this.state.IntelligentFilling})
    }

    getchiefComplain() {
        let txt = '';
        let values = this.props.form.getFieldsValue();
        if (this.state.IntelligentFilling) {
            //txt = txt + '目前出现了这些症状：' + (values.chiefComplain ? values.chiefComplain.join('、') : '') + (values.chiefComplainOthers ? values.chiefComplainOthers.join('、') : '')
            txt = (values.chiefComplain ? values.chiefComplain.join('、') : '') + (values.chiefComplainOthers ? values.chiefComplainOthers.join('、') : '')
        } else {
            // txt = txt + '目前出现了这些症状：' + (values.chiefComplain_input ? values.chiefComplain_input : '')
            txt = (values.chiefComplain_input ? values.chiefComplain_input : '')
        }

        return txt

    }

    getpastHistory() {
        let txt = '';
        let values = this.props.form.getFieldsValue();

        if (this.state.IntelligentFilling) {
            //txt = txt + '过去有过这些情况：' + (values.pastHistory ? values.pastHistory.join('、') : '')
            txt = (values.pastHistory ? values.pastHistory.join('、') : '')
        } else {
            // txt = txt + '过去有过这些情况：' + (values.pastHistory_input ? values.pastHistory_input : '')
            txt = (values.pastHistory_input ? values.pastHistory_input : '')
        }
        return txt
    }

    getOther() {

        let txt = '';
        let values = this.props.form.getFieldsValue();
        if (this.state.IntelligentFilling) {
            txt = values.other
        } else {

            txt = values.other_input
        }
        return txt
    }


    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div className={style.BasicDataForm}>
                <Row>
                    <Col span={8}>
                        <Form.Item
                            label="填写方式"
                        >
                            <Switch
                                onChange={this.changeFilling.bind(this)}
                                checkedChildren="智能病历"
                                unCheckedChildren="传统病历"
                                checked={this.state.IntelligentFilling}/>
                        </Form.Item>
                    </Col>
                </Row>
                <Form>
                    <Col span={16}>
                        <Row>
                            <Col span={8}>
                                <Form.Item
                                    label="姓名"
                                >
                                    {getFieldDecorator('name')(
                                        <Input style={{width: '100%'}}/>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    label="年龄"
                                    required
                                >
                                    {getFieldDecorator('age')(
                                        <InputNumber min={0} max={150} style={{width: '100%'}}/>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    label="性别"
                                    required
                                >
                                    {getFieldDecorator('sex', {initialValue: '男'})(
                                        <Select style={{width: '100%'}}>
                                            <Option value="男">男</Option>
                                            <Option value="女">女</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            {/* <Col span={8}>
                                <Form.Item
                                    label="填写日期"
                                >
                                    {getFieldDecorator('date')(
                                        <DatePicker style={{width: '100%'}} disabled/>
                                    )}
                                </Form.Item>
                            </Col>*/}
                        </Row>
                        {this.state.IntelligentFilling && <div>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="主诉症状"

                                    >
                                        {getFieldDecorator('chiefComplain', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '主诉症状',
                                                },
                                            ],
                                        })(
                                            <CheckboxGroup style={{width: '100%'}}>
                                                <Checkbox value="腹痛">腹痛</Checkbox>
                                                <Checkbox value="腹胀">腹胀</Checkbox>
                                                <Checkbox value="恶心呕吐">恶心呕吐</Checkbox>
                                                <Checkbox value="胸闷">胸闷</Checkbox>
                                                <Checkbox value="胸痛">胸痛</Checkbox>
                                                <Checkbox value="腹泻">腹泻</Checkbox>
                                                <Checkbox value="发热">发热</Checkbox>
                                                <Checkbox value="咳嗽">咳嗽</Checkbox>
                                                <Checkbox value="心慌">心慌</Checkbox>
                                            </ CheckboxGroup>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="其他主诉"
                                    >
                                        {getFieldDecorator('chiefComplainOthers')(
                                            <Select mode="tags" style={{width: '100%'}} placeholder="Tags Mode">
                                                <Option key='肚子疼'>肚子疼</Option>
                                                <Option key='肚子疼2'>肚子疼2</Option>
                                                <Option key='肚子疼3'>肚子疼3</Option>
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="既往史"
                                    >
                                        {getFieldDecorator('pastHistory')(
                                            <Select mode="tags" style={{width: '100%'}} placeholder="Tags Mode">
                                                <Option key='肚子疼'>肚子疼</Option>
                                                <Option key='肚子疼2'>肚子疼2</Option>
                                                <Option key='肚子疼3'>肚子疼3</Option>
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="其他"
                                    >
                                        {getFieldDecorator('other')(
                                            <TextArea style={{width: '100%'}}
                                                      autosize={{minRows: 2, maxRows: 3}}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>}
                        {!this.state.IntelligentFilling &&
                        <div>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="主诉"
                                        required
                                    >
                                        {getFieldDecorator('chiefComplain_input', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '主诉是必须的',
                                                },
                                            ],
                                        })(
                                            <TextArea style={{width: '100%'}}
                                                      autosize={{minRows: 2, maxRows: 3}}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="历史病历">
                                        {getFieldDecorator('pastHistory_input')(
                                            <TextArea style={{width: '100%'}}
                                                      autosize={{minRows: 2, maxRows: 3}}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Item
                                        label="其他">
                                        {getFieldDecorator('other_input')(
                                            <TextArea style={{width: '100%'}}
                                                      autosize={{minRows: 2, maxRows: 3}}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>
                        }
                        <Row>
                            <Button style={{marginLeft: 120}} type="primary"
                                    onClick={this.showConfirm.bind(this)}>开始诊断</Button>
                        </Row>
                    </Col>
                    <Col offset={1} span={7}>
                        <PatientCard
                            name={this.props.form.getFieldValue('name')}
                            sex={this.props.form.getFieldValue('sex')}
                            chiefComplain={this.getchiefComplain()}
                            pastHistory={this.getpastHistory()}
                            Other={this.getOther()}
                            style={{marginTop: -40}}/>
                    </Col>
                </Form>
            </div>

        );
    }
}

// export default Form.create()(InputForm)


const mapStateToProps = (state) => {
    return {...state}
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        AAD_SAVE_STATE: (data) => dispatch({type: 'AAD_SAVE_STATE', data: data}),
        AAD_RESET_STATE: () => dispatch({type: 'AAD_RESET_STATE'}),
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(InputForm));





