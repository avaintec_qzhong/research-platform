import React from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


class Example extends React.Component {
    componentDidMount() {
        console.log(this.props);
        setTimeout(() => {
            console.log('go');
            this.props.EXAMPLE_MINUS_ONE();
        }, 1000);
        setTimeout(() => {
            this.props.EXAMPLE_MINUS_ONE();
        }, 2000);
        setTimeout(() => {
            this.props.EXAMPLE_MINUS_ONE();
        }, 3000)
    }

    render() {
        return (<div>
            <p style={{color: 'red'}}>redux使用{this.props.example.count}</p>
        </div>)
    }
}

const mapStateToProps = (state) => {
    return {...state}
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        EXAMPLE_PLUS_ONE: () => dispatch({type: 'EXAMPLE_PLUS_ONE'}),
        EXAMPLE_MINUS_ONE: () => dispatch({type: 'EXAMPLE_MINUS_ONE'}),
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Example);