import React from 'react'
import Modelcard from '../../components/ModelCard/ModelCard.jsx'
import {Card} from 'antd';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import style from './Homepage.less'

//style={{backgroundColor: 'transparent'}}


const models = {
    cn: {
        title_recent: '最近使用',
        title_all: '辅助诊断工具集',
        models: [
            {name: '急腹症辅助诊断', path: '/aad'},
            {name: '肿瘤辅助诊断'},
            {name: '内科辅助诊断'},
            {name: 'xx辅助诊断'},
            {name: 'xx辅助诊断'},
            {name: 'xx辅助诊断'}
        ]
    },
    en: {
        title_recent: 'Recent use',
        title_all: 'Assisted Diagnostic Toolset',
        models: [
            {name: 'Auxiliary diagnosis of acute abdomen', path: '/aad'},
            {name: 'Auxiliary diagnosis of sepsis'},
            {name: 'Auxiliary diagnosis of sepsis'},
            {name: 'Auxiliary diagnosis of sepsis'},
            {name: 'Auxiliary diagnosis of sepsis'},
            {name: 'Auxiliary diagnosis of sepsis'}]
    },
    suomi: {
        title_recent: 'Viimeaikainen käyttö',
        title_all: 'Avustettu diagnostinen työkalusarja',
        models: [
            {name: 'Akuutin vatsan apudiagnoosi', path: '/aad'},
            {name: 'Lisädiagnoosi sepsiksestä'},
            {name: 'Lisädiagnoosi sepsiksestä'},
            {name: 'Lisädiagnoosi sepsiksestä'},
            {name: 'Lisädiagnoosi sepsiksestä'},
            {name: 'Lisädiagnoosi sepsiksestä'}
        ],
    }
};

class Homepage extends React.Component {
    componentDidMount() {
        console.log(1)
    }

    render() {

        return (
            <div className={style.Homepage}>
                <div style={{padding: 2, marginTop: 20}}>
                    <span className={style.classTitle}>{models[this.props.globalState.lang].title_recent}</span>
                    <div className={style.container}>
                        <Modelcard
                            title={models[this.props.globalState.lang].models[0].name}
                            path={models[this.props.globalState.lang].models[0].path}/>
                    </div>
                </div>
                <div style={{padding: 2}}>
                    <span className={style.classTitle}>{models[this.props.globalState.lang].title_all}</span>
                    <div className={style.container}>
                        {models[this.props.globalState.lang].models.map((obj, num) => (
                            <Modelcard title={obj.name} path={obj.path} key={num}/>
                        ))}
                    </div>
                </div>

            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {...state}
};

export default connect(mapStateToProps)(Homepage)