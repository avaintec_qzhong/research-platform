import React from 'react'
import style from './index.less'
import {Form, Icon, Input, Button, Checkbox, message} from 'antd';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {xhr_post} from "../../utils/http";

class LoginPage extends React.Component {
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);

                xhr_post("login", {
                    username: values.username,
                    password: values.password
                },(data)=>{
                    if(data.state===0){
                        this.props.AAD_SAVE_LOGIN({login: true});
                        this.props.router.push('/home')
                    }else{
                        message.error('请检查你的账号密码');
                    }
                });


                /*if (values.username === '123' && values.password === '123') {
                    this.props.AAD_SAVE_LOGIN({login: true});
                    this.props.router.push('/home')
                } else {
                    message.error('请检查你的账号密码');
                }*/
            }
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div className={style.login}>
                <img src={require('../../assets/img/ai.png')} className={style.ai}/>
                <div className={style.loginInput}>
                    <img src={require('../../assets/img/AvaintecLogo.png')} alt=""/>
                    <div className={style.loginForm}>
                        <Form>
                            <Form.Item>
                                {getFieldDecorator('username', {
                                    rules: [{required: true, message: 'Please input your username!'}],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                        placeholder="Username"
                                    />
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{required: true, message: 'Please input your Password!'}],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                        type="password"
                                        placeholder="Password"
                                    />
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(<Checkbox style={{color: '#fff'}}>Remember me</Checkbox>)}
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" onClick={this.handleSubmit.bind(this)} className={style.button}>
                                    登录
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {...state}
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        AAD_SAVE_LOGIN: (data) => dispatch({type: 'AAD_SAVE_LOGIN', data: data}),
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(LoginPage));