import React from 'react';
import {Card} from 'antd';
import showdown from 'showdown'

export default class showModuleInfo extends React.Component {
    componentDidMount() {
        //获取要转换的文字
        var text = `# 这里用markdown直接书写算法
# 杰卡德相似系数

两个集合A和B的交集元素在A，B的并集中所占的比例，称为两个集合的杰卡德相似系数，用符号J(A,B)表示。
 
杰卡德相似系数是衡量两个集合的相似度一种指标

给定两个n维二元向量A、B，A、B的每一维都只能是0或者1，利用Jaccard相似系数来计算二者的相似性：

1）  代表向量A与向量B都是0的维度个数；

2）   代表向量A是0而向量B是1的维度个数；

3） 代表向量A是1而向量B是0的维度个数；

4)   代表向量A和向量B都是1的维度个数。

n维向量的每一维都会落入这4类中的某一类，因此：
 
则Jaccard相似系数为
        `;
        //创建实例
        var converter = new showdown.Converter();
        //进行转换
        var html = converter.makeHtml(text);
        //展示到对应的地方  result便是id名称
        document.getElementById("result").innerHTML = html;
    }

    render() {
        return (
            <Card>
                <div id='result'>
                    showModuleInfo
                </div>
            </Card>
        )
    }
}



