import React from 'react'
import {Select, Radio, Icon, Card} from 'antd';
import Line from '../../../components/echartsShow/line'
import Histogram from '../../../components/echartsShow/Histogram'
import Bar from '../../../components/echartsShow/Bar'
import Scatter from '../../../components/echartsShow/Scatter'
import Radar from '../../../components/echartsShow/Radar'
import Heatmap from '../../../components/echartsShow/Heatmap'
import Graph from '../../../components/echartsShow/graph'
import style from './index.less'

const {Option} = Select;

const children = [];
for (let i = 10; i < 36; i++) {
    children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

export default class showTime extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showTable: false,
            sortType: 1,
            type: 'Graph'
        }
    }

    handleChange(value) {
        console.log(`selected ${value}`);
    }

    handleChangeIcon(e) {
        console.log('-', e)
    }

    chooseImg(value) {
        this.setState({
            type: value
        })

    }

    showChooseImg(numData) {
        switch (this.state.type) {
            case 'line':
                return < Line data={numData}/>;
            case 'Histogram':
                return < Histogram data={numData}/>;
            case 'Bar':
                return < Bar data={numData}/>;
            case 'Scatter':
                return < Scatter data={numData}/>;
            case 'Radar':
                return < Radar data={numData}/>;
            case 'Heatmap':
                return < Heatmap data={numData}/>;
            case 'Graph':
                return < Graph data={numData}/>;
            default :
                return < Line data={numData}/>;
        }
    }

    render() {
        return (
            <div className={style.showDataBox}>
                <div className={style.showType}>
                    <div className={style.icon}>
                        <Icon onClick={this.chooseImg.bind(this, 'Line')} type="line-chart"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Histogram')} type="bar-chart"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Bar')} type="pie-chart"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Scatter')} type="dot-chart"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Radar')} type="radar-chart"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Heatmap')} type="heat-map"/>
                        <Icon onClick={this.chooseImg.bind(this, 'Graph')} type="deployment-unit"/>
                    </div>
                    <div>
                        <span>选择要展示的列：</span>
                        <Select
                            size='small'
                            mode="multiple"
                            placeholder="Please select"
                            defaultValue={['a10', 'c12']}
                            onChange={this.handleChange.bind(this)}
                        >
                            {children}
                        </Select>
                    </div>
                </div>
                < div
                    className={style.showContent}>
                    {this.showChooseImg.bind(this)(this.props.numData)}
                </div>
            </div>
        )
    }
}


