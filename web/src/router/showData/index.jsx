import React from 'react'
import {Table, Divider, Tag, Tabs, Row, Radio, Select} from 'antd';
import WebWorker from '../../utils/workerSetup';
import getNumCol from '../../utils/getNumCol.js';
import ShowTime from './showTime/index';
import style from './index.less'
import OriginData from "../dataList";

import data from './test.json'

const {TabPane} = Tabs;

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Tags',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <span>{tags.map(tag => {
                let color = tag.length > 5 ? 'geekblue' : 'green';
                if (tag === 'loser') {
                    color = 'volcano';
                }
                return (
                    <Tag color={color} key={tag}>
                        {tag.toUpperCase()}
                    </Tag>
                );
            })}</span>
        ),
    },
    {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
            <span>
        <a>Invite {record.name}</a>
        <Divider type="vertical"/>
        <a>Delete</a>
      </span>
        ),
    },
];

/*const data = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];*/


export default class DataList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showTable: false,
            sortType: 1,
            data: data,
            numData: []
        }
    }

    getColnums() {
        if (this.state.data) {
            let firstRow = this.state.data[0];
            let temp = [];

            for (let key in firstRow) {
                temp.push({
                    title: key,
                    width: 100,
                    dataIndex: key,
                })
            }
            return temp

        } else {
            return columns
        }

    }

    componentDidMount() {
        this.getNumColums()
    }

    handleSizeChange(e) {
        this.setState({showTable: e.target.value});
    };

    handleSortChange(e) {
        this.setState({sortType: e.target.value});
    };

    getNumColums() {
        let worker = new WebWorker(getNumCol);
        worker.postMessage(data);
        worker.onmessage = (event) => {
            //console.log(event.data)
            this.setState({
                numData: event.data
            })
            worker.terminate();
        };
    }

    render() {
        return (
            <div className={style.dataShow}>
                <Tabs
                    size='small'
                    onChange={() => {
                    }} type="card">
                    <TabPane tab="表格" key="1">
                        <Table columns={this.getColnums.bind(this)()}
                               dataSource={this.state.data}
                               size="small"
                               pagination={{pageSize: 100}}/>
                    </TabPane>
                    <TabPane tab="可视化" key="2">
                        <ShowTime numData={this.state.numData}/>
                    </TabPane>
                </Tabs>
            </div>

        )
    }
}


