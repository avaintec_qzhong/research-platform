import React from 'react'
import {Card, Icon, Avatar, Col, Row} from 'antd';
import {hashHistory} from 'react-router';

const {Meta} = Card;


export default class GeneralCard extends React.Component {
    componentDidMount() {

    }

    goPath() {
        hashHistory.push('/ShowData')
    }

    render() {
        return (
            <Col span={4} style={{padding: 4}}>
                <Card
                    actions={[
                        <Icon type="search" key="setting" onClick={this.goPath.bind(this)}/>,
                        <Icon type="edit" key="edit"/>,
                    ]}
                >
                    <Meta
                        avatar={<Icon style={{color:'#ff4d4f',fontSize:30}} type="database" />}
                        title="新闻数据"
                        description="社会热点，用以计算社会热点"
                    />
                </Card>
            </Col>)
    }
}


