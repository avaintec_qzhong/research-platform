import React from 'react'
import style from './ModelCard.less'
import {Icon} from 'antd'
import {hashHistory} from 'react-router';

import {message, Tooltip} from 'antd';


export default class ModelCard extends React.Component {
    goPath() {
        //console.log(this.props)
        if (this.props.path) {
            hashHistory.push(this.props.path)
        } else {
            message.config({
                duration: 1.5,
                top: 100
            });
            message.warning(<span style={{fontSize: 16}}>该模型还在制作中</span>);
        }

    }

    getBLen(str) {
        if (str == null) return 0;
        if (typeof str != "string") {
            str += "";
        }
        let theStrLenth = str.replace(/[^\x00-\xff]/g, "01").length;
        if (theStrLenth > 17) {
            return str.substring(0, 14) + '...'
        } else {
            return str
        }


    }


    render() {
        return (
            <div style={{...this.props.style}}
                 onClick={this.goPath.bind(this)}
                 className={style.container}>
                <Icon type="codepen-circle" className={style.icon}/>
                <p className={style.modelName}>{this.getBLen(this.props.title)}</p>
            </div>
        )
    }
}