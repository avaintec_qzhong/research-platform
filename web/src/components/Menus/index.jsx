import {Layout, Menu, Icon} from 'antd';
import React from 'react'
import style from "../../layout/normalLayout.less";
import {Router, Route, hashHistory, Link, Redirect} from 'react-router';

const {Sider} = Layout;
const SubMenu = Menu.SubMenu;
import menusJson from './menus.json'
import DashboardCity from "../../router/Dashboard/city";

const type = [
    'book',
    'hdd',
    'table',
    'book',
    'hdd',
    'table',
]

export default class Menus extends React.Component {
    render() {
        return (
            <Sider
                trigger={null}
                collapsible
                collapsed={this.props.collapsed}
            >

                <div className={style.logo}>

                    {this.props.collapsed ? <img src={require('../../assets/img/AvaintecLogo.png')}
                                                 alt=""/> : 'SignHero Tailored'}
                </div>

                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <SubMenu
                        key="subx"
                        title={<span><Icon type="dashboard"/><span>宏观统计</span></span>}
                    >
                        <Menu.Item key="count1"><Link to='/DashboardCity'>市级</Link></Menu.Item>
                        <Menu.Item key="count2"> <Link to='/dashboardHospital'>院级</Link></Menu.Item>
                    </SubMenu>
                    {
                        menusJson.items[0].children.map((objs, index) => (
                            <SubMenu key={objs.id}
                                     title={<span><Icon type={type[index]}/><span>{objs.name}</span></span>}
                            >
                                {objs.children.map((obj_level2) => (
                                    <Menu.Item key={obj_level2.id}><Link to='/dataTable'>{obj_level2.name}</Link></Menu.Item>))}
                            </SubMenu>

                        ))
                    }
                </Menu>
            </Sider>
        )
    }
}