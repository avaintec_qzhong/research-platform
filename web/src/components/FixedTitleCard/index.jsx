import React from 'react'
import style from './index.less'
import {Icon} from 'antd'

export default class FixedTitleCard extends React.Component {
    render() {
        return (
            <div className={style.FixedTitleCard}>
                <div className={style.title}>
                    <span>{this.props.title}</span>
                    <Icon className={style.closeaction} onClick={this.props.close} type="close"/>
                </div>
                <div className={style.body}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}