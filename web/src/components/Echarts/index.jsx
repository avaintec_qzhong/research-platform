import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import echarts from 'echarts';

class ECharts extends Component {
    constructor(props) {
        super(props);
        this._onResize = this.onResize.bind(this)
    }

    onResize() {
        console.log(this.$echarts);
        this.$echarts.resize();
    };

    onClick(param) {
        const {onClick} = this.props;
        if (onClick) {
            onClick(param);
        }
    };

    componentDidMount() {
        setTimeout(() => {
            this.$echarts = echarts.init(ReactDOM.findDOMNode(this));
            this.updateDisplay();
            window.addEventListener('resize', this._onResize);
            console.log(this.$echarts)
        }, 50);
    }

    updateDisplay() {
        let {option} = this.props;
        if (option && this.$echarts) {
            this.$echarts.setOption(option, false);
        }

    }

    componentDidUpdate(prevProps) {
        this.updateDisplay();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._onResize);
        if (this.$echarts) {
            this.$echarts.dispose();
            this.$echarts = null;
        }
    }

    render() {
        let {style} = this.props;
        return <div style={style}/>;
    }
}

export default ECharts;
