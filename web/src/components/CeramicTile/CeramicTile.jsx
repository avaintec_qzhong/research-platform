import React from 'react'
import style from './CeramicTile.less'
import {Icon} from 'antd'

export default class CeramicTile extends React.Component {
    render() {
        return (
            <div style={{...this.props.style}} className={style.container}>
                <Icon type="codepen-circle" className={style.icon} />
                {this.props.title}
            </div>
        )
    }
}