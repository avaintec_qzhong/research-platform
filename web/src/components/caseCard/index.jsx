import React from 'react'
import {Card, Icon, Avatar, Col, Row} from 'antd';
import {hashHistory} from 'react-router';

const {Meta} = Card;


export default class caseCard extends React.Component {
    componentDidMount() {

    }

    goPath() {
        hashHistory.push('/OneItem')
    }

    render() {
        return (
            <Col span={4} style={{padding: 4}}>
                <Card
                    cover={
                        <img
                            alt="example"
                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                    }
                    actions={[
                        <Icon type="search" key="setting" onClick={this.goPath.bind(this)}/>,
                        <Icon type="edit" key="edit"/>,
                    ]}
                >
                    <Meta
                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                        title="xx项目"
                        description="项目描述"
                    />
                </Card>
            </Col>)
    }
}


