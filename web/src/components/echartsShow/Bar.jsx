import React, {Component} from 'react'
import Echarts from '../Echarts/index'


export default class Bar extends Component {
    getOption() {
        let legend = [];
        for (let key in this.props.data) {
            legend.push(key);
        }
        let seriesData = [];
        legend.forEach((key) => {
            seriesData.push({
                name: key,
                value: this.props.data[key].reduce((total, val) => (total + isNaN(val) ? Number(val) : 0))
            })
        });

        console.log(seriesData);

        let option = {
            title: {
                text: '某站点用户访问来源',
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                right: 'right',
                data: legend
            },
            series: [
                {
                    name: '数据权重',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: seriesData,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        return option
    }

    render() {
        return (
            <Echarts style={{width: '100%', height: '100%'}} option={this.getOption.bind(this)()}/>)
    }
}
