/* 结合了redux */
import React from 'react';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import {Router, Route, hashHistory, Link, Redirect, IndexRoute, Switch} from 'react-router';
import {syncHistoryWithStore, routerReducer} from 'react-router-redux';
import reducers from './reducers';


import routerconfig from './routerConfig'

// Add the reducer to your store on the `routing` key
const store = createStore(
    combineReducers({
        ...reducers,
        routing: routerReducer,
    })
);
// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(hashHistory, store);
const App = () => (
    <Provider store={store}>
        <Router history={history}>
            {routerconfig.map((obj, k) =>
                (<Route key={k} path={obj.path} component={obj.component}>
                    {obj.child.map((childObj, i) => (
                        (i === 0 && k === 0) ? [<IndexRoute
                            exact={true}
                            key={i}
                            component={childObj.component}
                        />, <Route
                            exact={true}
                            key={i}
                            path={childObj.path}
                            component={childObj.component}
                        />] : <Route
                            exact={true}
                            key={i}
                            path={childObj.path}
                            component={childObj.component}
                        />)
                    )}
                </Route>)
            )}
        </Router>
    </Provider>
);
export default App;


/*
(i === 0 && k === 0) ? <IndexRoute
    exact={true}
    key={i}
    component={childObj.component}
/> : <Route
    exact={true}
    key={i}
    path={childObj.path}
    component={childObj.component}
/>*/
