var fs = require('fs');
var csv = require('csv');
csv.from.stream(fs.createReadStream(__dirname + '/data.csv'))
    .to.path(__dirname + '/sample.out')
    .transform(function (row) {
        row.unshift(row.pop());
        return row;

    })
    .on('record', function (row, index) {

        console.log('#' + index + ' ' + JSON.stringify(row));

    })
    .on('close', function (count) {
        // when writing to a file, use the 'close' event
        // the 'end' event may fire before the file has been written
        console.log('Number of lines: ' + count);
    })
    .on('error', function (error) {

        console.log(error.message);

    })