module.exports = {
    devUrl: '/',
    devTestUrl: '/',
    productionUrl: '/'
};
/*
*路径在打包的时候会通过slice方法切割，/worldcup/.slice(0, -1)，返回/worldcup，
*
* 给所有的静态资源路径统一加上/worldcup
* */